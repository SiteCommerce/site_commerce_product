<?php

use Drupal\Core\Render\Element;
use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;

/**
 * Implements template_preprocess_site_commerce_product().
 */
function template_preprocess_site_commerce_product(&$variables) {
  $variables['view_mode'] = $variables['elements']['#view_mode'];
  $variables['site_commerce_product'] = $variables['elements']['#site_commerce_product'];
  $site_commerce_product = $variables['site_commerce_product'];

  // Helpful $content variable for templates.
  $variables['content'] = [];
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }

  // TODO: Данный код является устаревшей реализацией, но при необходимости можно получить изображения через него.
  // // Переменная для хранения изображений товара.
  // $variables['images'] = [];
  // $variables['images_links'] = [];
  // if (isset($variables['content']['field_media_image']['#items'])) {
  //   $style = \Drupal::entityTypeManager()->getStorage('image_style')->load('product_card_image');
  //   $variables['count_image'] = $variables['content']['field_media_image']['#items']->count();
  //   $i = 0;
  //   if ($variables['count_image']) {
  //     while ($i <= $variables['count_image'] - 1):
  //       $media = $variables['content']['field_media_image'][$i]['#media'];
  //       $media_field = $media->get('field_media_image')->first()->getValue();
  //       $file = File::load($media_field['target_id']);
  //       if ($file) {
  //         $uri = $file->getFileUri();
  //         $style_url = $style->buildUrl($uri);
  //         $url = file_create_url($uri);
  //         $variables['images_links'][$i] = ['style_url' => $style_url, 'url' => $url];
  //       }

  //       $variables['images'][$i] = $variables['content']['field_media_image'][$i];
  //       $i++;
  //     endwhile;
  //   }
  // }

  // Примечание над формой добавления в корзину.
  $variables['cart_description'] = '';
  if ($site_commerce_product->get('field_settings')->cart_description) {
    $variables['cart_description'] = $site_commerce_product->get('field_settings')->cart_description;
  }

  // Загружает блок с корзиной для текущего состояния покупки товара.
  $variables['cart'] = [];
  if ($site_commerce_product->get('field_settings')->cart_form_allow) {
    $variables['cart']['data'] = [
      '#lazy_builder' => ['site_commerce_product.lazy_builders:cartBlock', [$site_commerce_product->id()]],
      '#create_placeholder' => TRUE,
    ];

    // TODO: Временно отключен данный режим работы. Будет переписан.
    $variables['cart']['event'] = '';
    // // Загружает код события, которое произойдет при добавлении товара в корзину.
    // $type = $site_commerce_product->get('field_settings')->cart_after_adding_event;
    // $variables['cart']['event'] = [
    //   '#theme' => 'site_commerce_product_cart_block',
    //   '#site_commerce_product' => $site_commerce_product,
    //   '#type' => $type,
    // ];
  }
}

/**
 * Prepares variables for site-commerce-catalog-products.html.twig template.
 *
 * @param array $variables
 */
function template_preprocess_site_commerce_product_catalog_products(&$variables) {
  $site_commerce_products = $variables['site_commerce_products'];

  $variables['products'] = [];

  // Массив с полями для отображения.
  $fieldsToRender = [
    'field_summary_catalog', 'title', 'field_price_group',
  ];

  foreach ($site_commerce_products as $tid => $value) {
    $variables['products'][$tid]['name'] = $value['name'];
    $variables['products'][$tid]['summary'] = $value['summary'];
    $variables['products'][$tid]['view_all'] = $value['view_all'];
    $variables['products'][$tid]['count_term_products'] = $value['count_term_products'];
    $variables['products'][$tid]['tid'] = $value['tid'];
    $variables['products'][$tid]['count_products'] = 0;

    foreach ($value['site_commerce_product'] as $site_commerce_product) {
      $variables['products'][$tid]['items'][$site_commerce_product->id()]['site_commerce_product'] = $site_commerce_product;

      $display = \Drupal::service('entity_display.repository')->getViewDisplay('site_commerce_product', $site_commerce_product->bundle(), 'catalog');
      $viewBuilder = \Drupal::entityTypeManager()->getViewBuilder('site_commerce_product');

      foreach ($fieldsToRender as $field_name) {
        if (isset($site_commerce_product->{$field_name}) && $field = $site_commerce_product->{$field_name}) {
          $display_options = $display->getComponent($field_name);
          $variables['products'][$tid]['items'][$site_commerce_product->id()][$field_name] = "";
          if ($display_options) {
            $build = $viewBuilder->viewField($field, $display_options);
            if (!empty($build)) {
              $variables['products'][$tid]['items'][$site_commerce_product->id()][$field_name] = $build;
            }
          }
        }
      }

      // Форма добавления в корзину.
      $add_cart_form = [
        '#create_placeholder' => TRUE,
        '#lazy_builder' => ['site_commerce_product.lazy_builders:cartBlock', [$site_commerce_product->id(), TRUE]],
      ];
      $variables['products'][$tid]['items'][$site_commerce_product->id()]['add_cart_form'] = $add_cart_form;

      // Изображение позиции или изображение по умолчанию.
      // TODO: Оставлено в качестве примера вызова изображения по умолчанию простого поля изображения.
      // $file = $site_commerce_product->field_image->entity;
      // if (!$file) {
      //   $field_config = FieldConfig::loadByName('site_commerce_product', $site_commerce_product->bundle(), 'field_image');
      //   $file_uuid = $field_config->getSetting('default_image')['uuid'];
      //   if (!$file_uuid) {
      //     $field_storage_config = FieldStorageConfig::loadByName('site_commerce_product', 'field_image');
      //     $file_uuid = $field_storage_config->getSetting('default_image')['uuid'];
      //   }
      //   $file = \Drupal::service('entity.repository')->loadEntityByUuid('file', $file_uuid);
      // }

      if ($target_id = $site_commerce_product->field_media_image->target_id) {
        $media = Media::load($target_id);
        if ($media) {
          $media_field = $media->get('field_media_image')->first()->getValue();
          $file = File::load($media_field['target_id']);
          $variables['products'][$tid]['items'][$site_commerce_product->id()]['field_image'] = "";
          if ($file) {
            $variables['products'][$tid]['items'][$site_commerce_product->id()]['field_image'] = $file;
          }
        }
      }

      $variables['products'][$tid]['count_products'] = $variables['products'][$tid]['count_products'] + 1;
    }
  }
}

/**
 * Implements template_preprocess_HOOK() for list of available node type templates.
 */
function template_preprocess_site_commerce_product_add_list(&$variables) {
  if (!empty($variables['content'])) {
    foreach ($variables['content'] as $type) {
      $variables['types'][$type->id()]['label'] = $type->label();
      $variables['types'][$type->id()]['url'] = \Drupal::url('entity.site_commerce_product.add_form', ['site_commerce_product_type' => $type->id()]);
    }
  }
}

/**
 * Prepares variables for site-commerce-catalog.html.twig template.
 *
 * @param array $variables
 */
function template_preprocess_site_commerce_product_catalog(&$variables) {
  $terms = $variables['terms'];
  $vid = $variables['vid'];
  $viewmode = 'default';
  $entityType = 'taxonomy_term';
  $display = \Drupal::service('entity_display.repository')->getViewDisplay($entityType, $vid, $viewmode);
  $viewBuilder = \Drupal::entityTypeManager()->getViewBuilder($entityType);

  $variables['terms'] = [];

  $fieldsToRender = [
    'field_image', 'name',
  ];

  $variables['count_terms'] = 0;

  foreach ($terms as $term) {
    foreach ($fieldsToRender as $field_name) {
      if (isset($term->{$field_name}) && $field = $term->{$field_name}) {
        $fieldRenderable = $viewBuilder->viewField($field, $display->getComponent($field_name));
        if (count($fieldRenderable) && !empty($fieldRenderable)) {
          $variables['terms'][$term->id()][$field_name] = $fieldRenderable;
        }
      }
    }

    $variables['count_terms'] = $variables['count_terms'] + 1;
  }
}

/**
 * Prepares variables for site-commerce-catalog-list.html.twig template.
 *
 * @param array $variables
 */
function template_preprocess_site_commerce_product_catalog_list(&$variables, $hook) {
  $terms = $variables['terms'];
  $variables['terms'] = [];
  foreach ($terms as $term) {
    $variables['terms'][$term->id()]['name'] = $term->getName();
  }
}

/**
 * Prepares variables for site-commerce-catalog-children-list.html.twig template.
 *
 * @param array $variables
 */
function template_preprocess_site_commerce_product_catalog_children_list(&$variables, $hook) {
  $terms = $variables['terms'];
  $variables['terms'] = [];
  foreach ($terms as $term) {
    $variables['terms'][$term->id()]['name'] = $term->getName();
  }
}

/**
 * Prepares variables for site-commerce-product-products-catalog.html.twig template.
 *
 * @param array $variables
 */
function template_preprocess_site_commerce_product_products_catalog(&$variables) {
  $view = $variables['view'];
  $rows = $variables['rows'];

  // Аргументы views.
  $parametrs = $view->args[1];
  $is_child = $parametrs['child'];

  // Перечень товаров для отображения.
  $product_ids = [];
  $products = [];

  // Текущий термин таксономии.
  $tid = (int) $view->argument['field_category_target_id']->getValue();
  $term_storage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
  $term = $term_storage->load($tid);

  foreach ($rows as $id => $row) {
    $entity = $row['#site_commerce_product'];
    $product_ids[] = $entity->id();
  }

  if (count($product_ids)) {
    $count_term_products = \Drupal::service('site_commerce_product.database')->countProducts(1, $term->id());
    $products[$term->id()]['count_term_products'] = $count_term_products;

    // Заголовок категории.
    $products[$term->id()]['name'] = '';
    $products[$term->id()]['view_all'] = FALSE;
    $products[$term->id()]['tid'] = 0;

    if ($is_child) {
      $products[$term->id()]['name'] = $term->label();
      $products[$term->id()]['tid'] = $term->id();

      if ($count_term_products > 4) {
        $products[$term->id()]['view_all'] = TRUE;
      }
    }

    $products[$term->id()]['summary'] = $term->get('field_summary_catalog')->value;
    $products[$term->id()]['site_commerce_product'] = \Drupal::entityTypeManager()->getStorage('site_commerce_product')->loadMultiple($product_ids);
  }

  if (count($products)) {
    $variables['products'] = [
      '#theme' => 'site_commerce_product_catalog_products',
      '#site_commerce_products' => $products,
      '#attached' => [
        'library' => [
          'site_commerce_product/card',
          'site_commerce_product/catalog_products',
        ],
      ],
    ];
  }

}

/**
 * Implements template_preprocess_site_commerce_product_price_editor().
 */
function template_preprocess_site_commerce_product_price_editor(&$variables) {
  $entities = $variables['site_commerce_products'];

  $variables['products'] = [];
  foreach ($entities as $entity) {
    $price = $entity->getPrice();
    $variables['products'][$entity->id()] = [
      'label' => $entity->label(),
      'number_from' => $price['number_from'],
      'number' => $price['number'],
      'quantity_unit' => $entity->getQuantityUnit(),
    ];
  }

  // Единицы измерения количества.
  $variables['units'] = getUnitMeasurement();
}

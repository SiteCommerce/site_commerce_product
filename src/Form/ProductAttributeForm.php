<?php

namespace Drupal\site_commerce_product\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityConstraintViolationListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Base for handler for attribute entity edit forms.
 *
 * @internal
 */
class ProductAttributeForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $attribute = $this->entity;
    $group_storage = $this->entityTypeManager->getStorage('site_commerce_attribute_group');

    $attribute_storage = $this->entityTypeManager->getStorage('site_commerce_attribute');
    $group = $group_storage->load($attribute->bundle());

    $parent = array_keys($attribute_storage->loadParents($attribute->id()));
    $form_state->set(['attribute', 'parent'], $parent);
    $form_state->set(['attribute', 'group'], $group);

    $form['relations'] = [
      '#type' => 'details',
      '#title' => $this->t('Relations'),
      '#open' => FALSE,
      '#weight' => 10,
    ];

    $form['relations']['weight'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Weight'),
      '#size' => 6,
      '#default_value' => $attribute->getWeight(),
      '#description' => $this->t('Attributes are displayed in ascending order by weight.'),
      '#required' => TRUE,
    ];

    $form['gid'] = [
      '#type' => 'value',
      '#value' => $group->id(),
    ];

    $form['attribute_id'] = [
      '#type' => 'value',
      '#value' => $attribute->id(),
    ];

    return parent::form($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    // Ensure numeric values.
    if ($form_state->hasValue('weight') && !is_numeric($form_state->getValue('weight'))) {
      $form_state->setErrorByName('weight', $this->t('Weight value must be numeric.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntity(array $form, FormStateInterface $form_state) {
    $attribute = parent::buildEntity($form, $form_state);

    // Prevent leading and trailing spaces in attribute names.
    $attribute->setName(trim($attribute->getName()));

    // Assign parents with proper delta values starting from 0.
    $attribute->parent = array_values($form_state->getValue('parent'));

    return $attribute;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditedFieldNames(FormStateInterface $form_state) {
    return array_merge(['parent', 'weight'], parent::getEditedFieldNames($form_state));
  }

  /**
   * {@inheritdoc}
   */
  protected function flagViolations(EntityConstraintViolationListInterface $violations, array $form, FormStateInterface $form_state) {
    // Manually flag violations of fields not handled by the form display. This
    // is necessary as entity form displays only flag violations for fields
    // contained in the display.
    // @see ::form()
    foreach ($violations->getByField('parent') as $violation) {
      $form_state->setErrorByName('parent', $violation->getMessage());
    }
    foreach ($violations->getByField('weight') as $violation) {
      $form_state->setErrorByName('weight', $violation->getMessage());
    }

    parent::flagViolations($violations, $form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $attribute = $this->entity;

    $result = $attribute->save();

    $edit_link = $attribute->toLink($this->t('Edit'), 'edit-form')->toString();
    $view_link = $attribute->toLink()->toString();
    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('Created new attribute %attribute.', ['%attribute' => $view_link]));
        $this->logger('attribute')->notice('Created new attribute %attribute.', ['%attribute' => $attribute->getName(), 'link' => $edit_link]);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('Updated attribute %attribute.', ['%attribute' => $view_link]));
        $this->logger('attribute')->notice('Updated attribute %attribute.', ['%attribute' => $attribute->getName(), 'link' => $edit_link]);
        break;
    }

    $current_parent_count = count($form_state->getValue('parent'));
    // Root doesn't count if it's the only parent.
    if ($current_parent_count == 1 && $form_state->hasValue(['parent', 0])) {
      $form_state->setValue('parent', []);
    }

    $form_state->setValue('attribute_id', $attribute->id());
    $form_state->set('attribute_id', $attribute->id());
  }

}

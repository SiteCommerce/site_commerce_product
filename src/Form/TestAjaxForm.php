<?php

namespace Drupal\site_commerce_product\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AlertCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/** @package Drupal\site_commerce_product\Form */
class TestAjaxForm extends FormBase {

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'test_ajax_form';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Test value'),
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit1'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit button'),
      '#button_type' => 'primary',
    ];

    $form['actions']['submit2'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit ajax button'),
      '#button_type' => 'primary',
      '#ajax' => [
        'callback' => '::ajaxCallback',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * Ajax callback.
   */
  public function ajaxCallback(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    $response->addCommand(new AlertCommand("OK - Test ajax form"));

    return $response;
  }

}

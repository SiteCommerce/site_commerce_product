<?php

namespace Drupal\site_commerce_product\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\site_commerce_product\Ajax\ProductsLoadByCategories;

/**
 * Class implement form for loading products by checked categories.
 */
class ProductLoadByCategoriesForm extends FormBase {

  /**
   * Термины категорий для выбора.
   */
  protected $tids;

  /**
   * Ранее выбранные термины категорий.
   */
  protected $selected_tids;

  /**
   * Конструктор класса.
   * @param int $parent - родительский термин.
   * @return void
   */
  public function __construct(int $parent = 0) {
    $query = \Drupal::database()->select('taxonomy_term_field_data', 'n');
    $query->fields('n', ['tid', 'name']);
    $query->condition('n.vid', 'site_commerce_catalog');
    $query->orderBy('n.weight', 'ASC');
    $query->innerJoin('taxonomy_term__parent', 'h', 'h.entity_id=n.tid');
    $query->condition('h.parent_target_id', $parent);
    $query->condition('h.bundle', 'site_commerce_catalog');
    $query->innerJoin('taxonomy_term__field_view', 'v', 'v.entity_id=n.tid');
    $query->condition('v.bundle', 'site_commerce_catalog');
    $query->condition('v.field_view_value', 1);
    $this->tids = $query->execute()->fetchAllKeyed();

    $session = \Drupal::service('session');
    $data = $session->get('selected_tids');
    if (empty($data->tids)) {
      $this->selected_tids = [];
    } else {
      $this->selected_tids = $data->tids;
    }
  }

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'site_commerce_product_load_by_categories_form';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Класс для элементов формы.
    $form_class = Html::getClass($this->getFormId());

    // Обертка элементов формы.
    $form['wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => $form_class . '__wrapper',
      ],
    ];

    $form['wrapper']['categories'] = [
      '#type' => 'checkboxes',
      '#options' => $this->tids,
      '#title' => $this->t('Product catalog'),
      '#default_value' =>  $this->selected_tids,
    ];

    // Добавляем нашу кнопку для отправки.
    $form['wrapper']['actions']['#type'] = 'actions';
    $form['wrapper']['actions']['#attributes'] = ['class' => [$form_class . '__actions']];

    // Кнопка Применить.
    $form['wrapper']['actions']['load_products'] = [
      '#type' => 'submit',
      '#id' => $form_class . '__load-products',
      '#value' => $this->t('Apply'),
      '#button_type' => 'primary',
      '#ajax' => [
        'callback' => '::ajaxCallback',
      ],
      '#attributes' => [
        'class' => [$form_class . '__btn', $form_class . '__btn-load-products'],
      ],
    ];

    $form['#attached']['library'][] = 'site_commerce_product/catalog_products';
    $form['#attached']['library'][] = 'site_commerce_product/products_load_by_categories';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * Ajax load products callback.
   */
  public function ajaxCallback(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    // Если нет ошибок валидации.
    if (!$form_state->hasAnyErrors()) {
      $categories = $form_state->getValue('categories');
      $tids = [];
      foreach ($categories as $key => $value) {
        if ($value) {
          $tids[] = (int) $key;
        }
      }
    }

    if (count($tids)) {
      $session = \Drupal::service('session');
      $data = [
        'id' => '#products-load',
        'tids' => $tids,
      ];
      $session->set('selected_tids', (object) $data);
      $response->addCommand(new ProductsLoadByCategories($data['id'], $data['tids']));
    }

    return $response;
  }
}

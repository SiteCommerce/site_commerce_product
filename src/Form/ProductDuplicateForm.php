<?php

namespace Drupal\site_commerce_product\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a form for duplicate a Product entities.
 *
 * @ingroup site_commerce_product
 */
class ProductDuplicateForm extends ContentEntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to duplicate entity %name?', array('%name' => $this->entity->label()));
  }

  /**
   * {@inheritdoc}
   *
   * If the delete command is canceled, return to the Product list.
   */
  public function getCancelURL() {
    return new Url('entity.site_commerce_product.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Duplicate');
  }

  /**
   * {@inheritdoc}
   *
   * Delete the entity and log the event. log() replaces the watchdog.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entity = $this->getEntity();

    $duplicate = $entity->createDuplicate();
    $duplicate->setTitle($this->t('Duplicate') . ": " . $duplicate->label());
    $duplicate->save();

    $form_state->setRedirect('entity.site_commerce_product.collection');
  }

}

<?php

namespace Drupal\site_commerce_product\Form;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\site_commerce_product\Entity\ProductAttributeGroupInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides overview form for a attributes group.
 *
 * @internal
 */
class ProductAttributeGroupOverviewForm extends FormBase {

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The attribute storage handler.
   *
   * @var \Drupal\Core\Entity\Sql\SqlContentEntityStorage
   */
  protected $storageController;

  /**
   * The attribute list builder.
   *
   * @var \Drupal\Core\Entity\EntityListBuilderInterface
   */
  protected $attributeListBuilder;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * Constructs an ProductAttributesOverview object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   */
  public function __construct(ModuleHandlerInterface $module_handler, EntityTypeManagerInterface $entity_type_manager) {
    $this->moduleHandler = $module_handler;
    $this->entityTypeManager = $entity_type_manager;
    $this->storageController = $entity_type_manager->getStorage('site_commerce_attribute');
    $this->attributeListBuilder = $entity_type_manager->getListBuilder('site_commerce_attribute');
    $this->renderer = \Drupal::service('renderer');
    $this->entityRepository = \Drupal::service('entity.repository');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'site_commerce_attribute_group_overview_form';
  }

  /**
   * Form constructor.
   *
   * Display a tree of all the attributes in a group, with options to edit
   * each one. The form is made drag and drop by the theme function.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\site_commerce_product\Entity\ProductAttributeGroupInterface $group
   *   The group to display the overview form for.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state, ProductAttributeGroupInterface $site_commerce_attribute_group = NULL) {

    $group = $site_commerce_attribute_group;
    $group_hierarchy = $this->storageController->getGroupHierarchyType($group->id());

    $form_state->set(['attribute', 'group'], $group);
    $errors = $form_state->getErrors();

    $row_position = 0;

    // Elements shown.
    $page_entries = 0;

    // Build the actual form.
    $access_control_handler = $this->entityTypeManager->getAccessControlHandler('site_commerce_attribute');
    $create_access = $access_control_handler->createAccess($group->id(), NULL, [], TRUE);
    if ($create_access->isAllowed()) {
      $empty = $this->t('No attributes. <a href=":link">Add attribute</a>.', [':link' => Url::fromRoute('entity.site_commerce_attribute.add_form', ['site_commerce_attribute_group' => $group->id()])->toString()]);
    } else {
      $empty = $this->t('No attributes available.');
    }

    $form['attributes'] = [
      '#type' => 'table',
      '#empty' => $empty,
      '#header' => [
        'attribute' => $this->t('Name'),
        'operations' => $this->t('Operations'),
        'weight' => $this->t('Weight'),
      ],
      '#attributes' => [
        'id' => 'attribute',
      ],
    ];
    $this->renderer->addCacheableDependency($form['attributes'], $create_access);

    // An array of the attributes to be displayed on this page.
    $current_page = [];

    $delta = 0;
    $attribute_deltas = [];
    $tree = $this->storageController->loadTree($group->id(), 0, NULL, TRUE);
    $tree_index = 0;
    do {
      // In case this tree is completely empty.
      if (empty($tree[$tree_index])) {
        break;
      }
      $delta++;

      $attribute = $tree[$tree_index];

      // Keep track of the first term displayed on this page.
      if ($page_entries == 1) {
        $form['#first_aid'] = $attribute->id();
      }

      $attribute_deltas[$attribute->id()] = isset($attribute_deltas[$attribute->id()]) ? $attribute_deltas[$attribute->id()] + 1 : 0;
      $key = 'tid:' . $attribute->id() . ':' . $attribute_deltas[$attribute->id()];

      $current_page[$key] = $attribute;
    } while (isset($tree[++$tree_index]));

    // Only allow access to change parents and reorder the tree if there are no
    // attributes with multiple parents.
    $update_tree_access = AccessResult::allowedIf($group_hierarchy !== ProductAttributeGroupInterface::HIERARCHY_MULTIPLE);

    foreach ($current_page as $key => $attribute) {
      $form['attributes'][$key] = [
        'attribute' => [],
        'operations' => [],
        'weight' => $update_tree_access->isAllowed() ? [] : NULL,
      ];
      /** @var $attribute \Drupal\Core\Entity\EntityInterface */
      $attribute = $this->entityRepository->getTranslationFromContext($attribute);
      $form['attributes'][$key]['#attribute'] = $attribute;
      $indentation = [];
      if (isset($attribute->depth) && $attribute->depth > 0) {
        $indentation = [
          '#theme' => 'indentation',
          '#size' => $attribute->depth,
        ];
      }
      $form['attributes'][$key]['attribute'] = [
        '#prefix' => !empty($indentation) ? $this->renderer->render($indentation) : '',
        '#type' => 'link',
        '#title' => $attribute->getName(),
        '#url' => $attribute->toUrl(),
      ];

      $form['attributes'][$key]['#attributes']['class'] = [];

      if ($update_tree_access->isAllowed() && count($tree) > 1) {
        $parent_fields = TRUE;
        $form['attributes'][$key]['attribute']['tid'] = [
          '#type' => 'hidden',
          '#value' => $attribute->id(),
          '#attributes' => [
            'class' => ['attribute-id'],
          ],
        ];
        $form['attributes'][$key]['attribute']['parent'] = [
          '#type' => 'hidden',
          // Yes, default_value on a hidden. It needs to be changeable by the
          // javascript.
          '#default_value' => $attribute->parents[0],
          '#attributes' => [
            'class' => ['attribute-parent'],
          ],
        ];
        $form['attributes'][$key]['attribute']['depth'] = [
          '#type' => 'hidden',
          // Same as above, the depth is modified by javascript, so it's a
          // default_value.
          '#default_value' => $attribute->depth,
          '#attributes' => [
            'class' => ['attribute-depth'],
          ],
        ];
      }
      $update_access = $attribute->access('update', NULL, TRUE);
      $update_tree_access = $update_tree_access->andIf($update_access);

      if ($update_tree_access->isAllowed()) {
        $form['attributes'][$key]['weight'] = [
          '#type' => 'weight',
          '#delta' => $delta,
          '#title' => $this->t('Weight for added attribute'),
          '#title_display' => 'invisible',
          '#default_value' => $attribute->getWeight(),
          '#attributes' => ['class' => ['attribute-weight']],
        ];
      }

      if ($operations = $this->attributeListBuilder->getOperations($attribute)) {
        $form['attributes'][$key]['operations'] = [
          '#type' => 'operations',
          '#links' => $operations,
        ];
      }

      if ($parent_fields) {
        $form['attributes'][$key]['#attributes']['class'][] = 'draggable';
      }

      // Add an error class if this row contains a form error.
      foreach ($errors as $error_key => $error) {
        if (strpos($error_key, $key) === 0) {
          $form['attributes'][$key]['#attributes']['class'][] = 'error';
        }
      }
      $row_position++;
    }

    $this->renderer->addCacheableDependency($form['attributes'], $update_tree_access);
    if ($update_tree_access->isAllowed()) {
      if ($parent_fields) {
        $form['attributes']['#tabledrag'][] = [
          'action' => 'match',
          'relationship' => 'parent',
          'group' => 'attribute-parent',
          'subgroup' => 'attribute-parent',
          'source' => 'attribute-id',
          'hidden' => FALSE,
        ];
        $form['attributes']['#tabledrag'][] = [
          'action' => 'depth',
          'relationship' => 'group',
          'group' => 'attribute-depth',
          'hidden' => FALSE,
        ];
      }
      $form['attributes']['#tabledrag'][] = [
        'action' => 'order',
        'relationship' => 'sibling',
        'group' => 'attribute-weight',
      ];
    }

    if ($update_tree_access->isAllowed() && count($tree) > 1) {
      $form['actions'] = ['#type' => 'actions', '#tree' => FALSE];
      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Save'),
        '#button_type' => 'primary',
      ];
    }

    return $form;
  }

  /**
   * Form submission handler.
   *
   * Rather than using a textfield or weight field, this form depends entirely
   * upon the order of form elements on the page to deattributeine new weights.
   *
   * Because there might be hundreds or thousands of taxonomy attributes that need to
   * be ordered, attributes are weighted from 0 to the number of attributes in the
   * group, rather than the standard -10 to 10 scale. Numbers are sorted
   * lowest to highest, but are not necessarily sequential. Numbers may be
   * skipped when a attribute has children so that reordering is minimal when a child
   * is added or removed from a attribute.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Sort attribute order based on weight.
    uasort($form_state->getValue('attributes'), ['Drupal\Component\Utility\SortArray', 'sortByWeightElement']);

    $group = $form_state->get(['attribute', 'group']);
    $changed_attributes = [];
    $tree = $this->storageController->loadTree($group->id(), 0, NULL, TRUE);

    if (empty($tree)) {
      return;
    }

    // Renumber the current page weights and assign any new parents.
    $weight = 0;
    $level_weights = [];
    foreach ($form_state->getValue('attributes') as $tid => $values) {
      if (isset($form['attributes'][$tid]['#attribute'])) {
        $attribute = $form['attributes'][$tid]['#attribute'];
        // Give attributes at the root level a weight in sequence with attributes on previous pages.
        if ($values['attribute']['parent'] == 0 && $attribute->getWeight() != $weight) {
          $attribute->setWeight($weight);
          $changed_attributes[$attribute->id()] = $attribute;
        }
        // Terms not at the root level can safely start from 0 because they're all on this page.
        elseif ($values['attribute']['parent'] > 0) {
          $level_weights[$values['attribute']['parent']] = isset($level_weights[$values['attribute']['parent']]) ? $level_weights[$values['attribute']['parent']] + 1 : 0;
          if ($level_weights[$values['attribute']['parent']] != $attribute->getWeight()) {
            $attribute->setWeight($level_weights[$values['attribute']['parent']]);
            $changed_attributes[$attribute->id()] = $attribute;
          }
        }
        // Update any changed parents.
        if ($values['attribute']['parent'] != $attribute->parents[0]) {
          $attribute->parent->target_id = $values['attribute']['parent'];
          $changed_attributes[$attribute->id()] = $attribute;
        }
        $weight++;
      }
    }

    // Build a list of all attributes that need to be updated on following pages.
    for ($weight; $weight < count($tree); $weight++) {
      $attribute = $tree[$weight];
      if ($attribute->parents[0] == 0 && $attribute->getWeight() != $weight) {
        $attribute->parent->target_id = $attribute->parents[0];
        $attribute->setWeight($weight);
        $changed_attributes[$attribute->id()] = $attribute;
      }
    }

    if (!empty($changed_attributes)) {
      // Save all updated attributes.
      foreach ($changed_attributes as $attribute) {
        $attribute->save();
      }

      $this->messenger()->addStatus($this->t('The configuration options have been saved.'));
    }
  }

}

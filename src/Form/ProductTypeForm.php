<?php

namespace Drupal\site_commerce_product\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler for the Product type add and edit forms.
 */
class ProductTypeForm extends EntityForm {

  /**
   * Constructs an site_commerce_productForm object.
   *
   * @param \Drupal\Core\Entity\Query\QueryFactory $entity_query
   *   The entity query.
   */
  public function __construct(QueryFactory $entity_query) {
    $this->entityQuery = $entity_query;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.query')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $entity = $this->entity;

    $form['label'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $entity->label(),
      '#required' => TRUE,
    );

    $form['id'] = array(
      '#type' => 'machine_name',
      '#default_value' => $entity->id(),
      '#machine_name' => array(
        'exists' => array($this, 'exist'),
      ),
      '#disabled' => !$entity->isNew(),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $site_commerce_product = $this->entity;
    $status = $site_commerce_product->save();

    if ($status) {
      drupal_set_message($this->t('Saved the %label site_commerce_product.', array(
        '%label' => $site_commerce_product->label(),
      )));
    }
    else {
      drupal_set_message($this->t('The %label site_commerce_product was not saved.', array(
        '%label' => $site_commerce_product->label(),
      )));
    }

    $form_state->setRedirect('entity.site_commerce_product_type.collection');
  }

  /**
   * Helper function to check whether an ProductType configuration entity exists.
   */
  public function exist($id) {
    $entity = $this->entityQuery->get('site_commerce_product_type')
      ->condition('id', $id)
      ->execute();
    return (bool) $entity;
  }
}

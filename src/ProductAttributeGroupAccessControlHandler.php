<?php

namespace Drupal\site_commerce_product;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the product attributes group entity.
 *
 * @see \Drupal\site_commerce_product\Entity\ProductAttributeGroup
 */
class ProductAttributeGroupAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermissions($account, ['administer site_commerce_attribute']);

      default:
        return parent::checkAccess($entity, $operation, $account);
    }
  }

}

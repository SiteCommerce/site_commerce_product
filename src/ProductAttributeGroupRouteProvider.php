<?php

namespace Drupal\site_commerce_product;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Symfony\Component\Routing\Route;

/**
 * Route provider for attributes group entity.
 */
class ProductAttributeGroupRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {

    $collection = parent::getRoutes($entity_type);

    if ($overview_page_route = $this->getOverviewPageRoute($entity_type)) {
      $collection->add("entity.site_commerce_attribute_group.overview_form", $overview_page_route);
    }

    return $collection;
  }

  /**
   * Gets the overview page route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getOverviewPageRoute(EntityTypeInterface $entity_type) {
    $route = new Route('/admin/site-commerce/products/attributes-groups/{site_commerce_attribute_group}/overview');
    $route->setDefault('_title_callback', 'Drupal\site_commerce_product\Controller\ProductAttributeController::groupTitle');
    $route->setDefault('_form', 'Drupal\site_commerce_product\Form\ProductAttributeGroupOverviewForm');
    $route->setRequirement('_permission', 'administer site_commerce_attribute');
    $route->setOption('_admin_route', TRUE);

    return $route;
  }

}

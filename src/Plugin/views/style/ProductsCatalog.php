<?php

namespace Drupal\site_commerce_product\Plugin\views\style;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;

/**
 * A Views style that renders markup for products in catalog.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "site_commerce_product_products_catalog",
 *   title = @Translation("Products in the catalog"),
 *   theme = "site_commerce_product_products_catalog",
 *   display_types = {"normal"}
 * )
 */
class ProductsCatalog extends StylePluginBase {

  /**
   * Does this Style plugin allow Row plugins?
   *
   * @var bool
   */
  protected $usesRowPlugin = TRUE;

  /**
   * Does the Style plugin support grouping of rows?
   *
   * @var bool
   */
  protected $usesGrouping = FALSE;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
  }

}


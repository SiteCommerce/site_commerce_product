<?php

namespace Drupal\site_commerce_product\Plugin\Search;

use Drupal\Core\Access\AccessibleInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Config\Config;
use Drupal\Core\Database\Connection;
use Drupal\Core\Session\AccountInterface;
use Drupal\search\Plugin\SearchIndexingInterface;
use Drupal\search\Plugin\SearchPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Executes a keyword search for users against the {users} database table.
 *
 * @SearchPlugin(
 *   id = "site_commerce_product_search",
 *   title = @Translation("Products")
 * )
 */
class ProductSearch extends SearchPluginBase implements AccessibleInterface, SearchIndexingInterface {

  /**
   * A database connection object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * A config object for 'search.settings'.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $searchSettings;

  /**
   * {@inheritdoc}
   */
  static public function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('database'),
      $container->get('config.factory')->get('search.settings'),
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * Constructs a \Drupal\node\Plugin\Search\NodeSearch object.
   * @param \Drupal\Core\Database\Connection $database
   *   A database connection object.
   */
  public function __construct(Connection $database, Config $search_settings, array $configuration, $plugin_id, $plugin_definition) {
    $this->database = $database;
    $this->searchSettings = $search_settings;
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function access($operation = 'view', AccountInterface $account = NULL, $return_as_object = FALSE) {
    $result = AccessResult::neutral();
    return $return_as_object ? $result : $result->isAllowed();
  }

  /**
   * {@inheritdoc}
   */
  public function indexStatus() {
    $total = $this->database->query("SELECT COUNT(*) FROM {site_commerce_product} n WHERE n.type='education'")->fetchField();
    $remaining = $this->database->query("SELECT COUNT(DISTINCT n.course_esid) FROM {site_commerce_product} n LEFT JOIN {search_dataset} sd ON sd.sid = n.course_esid AND sd.type = :type WHERE n.type = 'education' AND sd.sid IS NULL OR sd.reindex <> 0", array(':type' => $this->getPluginId()))->fetchField();
    return array('remaining' => $remaining, 'total' => $total);
  }

  /**
   * {@inheritdoc}
   */
  public function indexClear() {
    search_index_clear($this->getPluginId());
  }

  /**
   * {@inheritdoc}
   */
  public function markForReindex() {
    search_mark_for_reindex($this->getPluginId());
  }

  /**
   * {@inheritdoc}
   */
  public function updateIndex() {
    $limit = (int) $this->searchSettings->get('index.cron_limit');

    $query = $this->database->select('site_commerce_product', 'n');
    $query->addField('n', 'course_esid');
    $query->condition('n.type', 'education');
    $query->leftJoin('search_dataset', 'sd', 'sd.sid = n.course_esid AND sd.type = :type', array(':type' => $this->getPluginId()));
    $query->addExpression('CASE MAX(sd.reindex) WHEN NULL THEN 0 ELSE 1 END', 'ex');
    $query->addExpression('MAX(sd.reindex)', 'ex2');
    $query->condition(
      $query->orConditionGroup()
        ->where('sd.sid IS NULL')
        ->condition('sd.reindex', 0, '<>')
    );
    $query->orderBy('ex', 'DESC')
      ->orderBy('ex2')
      ->orderBy('n.course_esid')
      ->groupBy('n.course_esid')
      ->range(0, $limit);

    $results = $query->execute()->fetchCol();
    if (!$results) {
      return;
    }

    $databaseCourses = \Drupal::service('site_kadry_courses.database');

    foreach ($results as $course_esid) {
      $step = (object) $databaseCourses->loadStep($course_esid);
      $this->indexStep($step);
    }
  }

  /**
   * Indexes a single step.
   *
   * @param $step
   *   The step to index.
   */
  protected function indexStep($step) {
    $text = '<h1>' . $step->title . '</h1> ';
    $text .= $step->description . ' ';

    $text = trim($text);

    // Use the current default interface language.
    $language = \Drupal::languageManager()->getCurrentLanguage();

    // Instantiate the transliteration class.
    $transliteration = \Drupal::service('transliteration');

    // Use this to transliterate some text.
    $text_transliterate = $transliteration->transliterate($text, $language->getId());
    $text = $text . $text_transliterate . ' ';

    // Fetch extra data normally not visible.
    $extra = \Drupal::moduleHandler()->invokeAll('step_update_index', [$step]);
    foreach ($extra as $t) {
      $text .= $t;
    }

    // Update index, using search index "type" equal to the plugin ID.
    search_index($this->getPluginId(), $step->course_esid, $language->getId(), $text);
  }

  /**
   * {@inheritdoc}
   */
  public function execute() {
    $results = array();
    return $results;
  }
}

<?php

namespace Drupal\site_commerce_product\Plugin\Field\FieldWidget;

use Drupal;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the Product entities attribute default widget.
 *
 * @FieldWidget(
 *   id = "site_commerce_product_attribute_default",
 *   module = "site_commerce_product",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "site_commerce_product_attribute"
 *   }
 * )
 */
class ProductAttributeDefault extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = [];

    $element['gid'] = [
      '#type' => 'select',
      '#title' => $this->t('New product'),
      '#options' => [
        0 => $this->t('No'),
        1 => $this->t('Yes'),
      ],
      '#default_value' => isset($items[$delta]->gid) ? $items[$delta]->gid : 0,
      '#theme_wrappers' => [],
    ];

    $element['aid'] = [
      '#type' => 'select',
      '#title' => $this->t('Appearance'),
      '#options' => getAppearance(),
      '#default_value' => isset($items[$delta]->aid) ? $items[$delta]->aid : 0,
      '#empty_value' => 0,
      '#theme_wrappers' => [],
    ];

    $element['image_target_id'] = [
      '#type' => 'media_library',
      '#allowed_bundles' => ['product_image'],
      '#title' => t('Upload your image'),
      '#default_value' => NULL,
      '#description' => t('Upload or select your profile image.'),
    ];

    $element['quantity_unit'] = [
      '#type' => 'select',
      '#title' => $this->t('Unit of quantity measurement'),
      '#options' => getUnitMeasurement(),
      '#default_value' => isset($items[$delta]->quantity_unit) ? $items[$delta]->quantity_unit : 'шт',
      '#empty_value' => '',
      '#theme_wrappers' => [],
    ];

    // $element['#theme'] = 'site_commerce_product_attribute_default';

    return $element;
  }
}

<?php

namespace Drupal\site_commerce_product\Plugin\Field\FieldWidget;

use Drupal;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the Product entities settings default widget.
 *
 * @FieldWidget(
 *   id = "site_commerce_product_settings_default",
 *   module = "site_commerce_product",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "site_commerce_product_settings"
 *   }
 * )
 */
class ProductSettingsDefault extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = [];

    // Настройки отображения товара.
    $element['status_new'] = [
      '#type' => 'select',
      '#title' => $this->t('New product'),
      '#options' => [
        0 => $this->t('No'),
        1 => $this->t('Yes'),
      ],
      '#default_value' => isset($items[$delta]->status_new) ? $items[$delta]->status_new : 0,
      '#theme_wrappers' => [],
    ];

    $element['status_appearance'] = [
      '#type' => 'select',
      '#title' => $this->t('Appearance'),
      '#options' => getAppearance(),
      '#default_value' => isset($items[$delta]->status_appearance) ? $items[$delta]->status_appearance : 0,
      '#empty_value' => 0,
      '#theme_wrappers' => [],
    ];

    $element['status_stock'] = [
      '#type' => 'select',
      '#title' => $this->t('Stock availability'),
      '#options' => getStockAvailability(),
      '#default_value' => isset($items[$delta]->status_stock) ? $items[$delta]->status_stock : 0,
      '#empty_value' => 0,
      '#theme_wrappers' => [],
    ];

    // Настройки количества товара.
    $element['quantity_stock_allow'] = [
      '#type' => 'select',
      '#title' => $this->t('Allow displaying the quantity in stock'),
      '#options' => [
        0 => $this->t('No'),
        1 => $this->t('Yes'),
      ],
      '#default_value' => isset($items[$delta]->quantity_stock_allow) ? $items[$delta]->quantity_stock_allow : 0,
      '#theme_wrappers' => [],
    ];

    $element['quantity_stock_value'] = [
      '#type' => 'number',
      '#title' => $this->t('Quantity in stock'),
      '#default_value' => isset($items[$delta]->quantity_stock_value) ? $items[$delta]->quantity_stock_value : 0,
      '#empty_value' => 0,
      '#step' => 1,
      '#min' => 0,
      '#theme_wrappers' => [],
    ];

    $element['quantity_unit'] = [
      '#type' => 'select',
      '#title' => $this->t('Unit of quantity measurement'),
      '#options' => getUnitMeasurement(),
      '#default_value' => isset($items[$delta]->quantity_unit) ? $items[$delta]->quantity_unit : 'шт',
      '#empty_value' => '',
      '#theme_wrappers' => [],
    ];

    $element['status_type_sale'] = [
      '#type' => 'select',
      '#title' => $this->t('Type of sale'),
      '#options' => [
        1 => $this->t('Piece sales'),
        2 => $this->t('Sale as part of another product'),
      ],
      '#default_value' => isset($items[$delta]->status_type_sale) ? $items[$delta]->status_type_sale : 1,
      '#theme_wrappers' => [],
    ];

    $element['quantity_order_min'] = [
      '#type' => 'number',
      '#title' => $this->t('Minimum order quantity'),
      '#default_value' => isset($items[$delta]->quantity_order_min) ? $items[$delta]->quantity_order_min : 1,
      '#empty_value' => 1,
      '#step' => 1,
      '#min' => 1,
      '#theme_wrappers' => [],
    ];

    $element['quantity_order_max'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum order quantity'),
      '#default_value' => isset($items[$delta]->quantity_order_max) ? $items[$delta]->quantity_order_max : 0,
      '#empty_value' => 0,
      '#step' => 1,
      '#min' => 0,
      '#theme_wrappers' => [],
    ];

    // Настройки добавления товара в корзину.
    $element['cart_form_allow'] = [
      '#type' => 'select',
      '#title' => $this->t('Allow displaying the add to cart form'),
      '#options' => [
        0 => $this->t('No'),
        1 => $this->t('Yes'),
      ],
      '#default_value' => isset($items[$delta]->cart_form_allow) ? $items[$delta]->cart_form_allow : 1,
      '#theme_wrappers' => [],
    ];

    $element['cart_quantity_allow'] = [
      '#type' => 'select',
      '#title' => $this->t('Allow changing the quantity of products in the cart'),
      '#options' => [
        0 => $this->t('No'),
        1 => $this->t('Yes'),
      ],
      '#default_value' => isset($items[$delta]->cart_quantity_allow) ? $items[$delta]->cart_quantity_allow : 1,
      '#theme_wrappers' => [],
    ];

    $element['cart_after_adding_event'] = [
      '#type' => 'select',
      '#title' => $this->t('Event after adding to cart'),
      '#options' => getEventAddCart(),
      '#default_value' => isset($items[$delta]->cart_after_adding_event) ? $items[$delta]->cart_after_adding_event : 'load_goto_order_checkout_form',
      '#theme_wrappers' => [],
    ];

    $element['cart_label_button_add'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button name adding to cart'),
      '#default_value' => isset($items[$delta]->cart_label_button_add) ? $items[$delta]->cart_label_button_add : '',
      '#maxlength' => 255,
      '#theme_wrappers' => [],
    ];

    $element['cart_label_button_click'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button name buy in one click'),
      '#default_value' => isset($items[$delta]->cart_label_button_click) ? $items[$delta]->cart_label_button_click : '',
      '#maxlength' => 255,
      '#theme_wrappers' => [],
    ];

    $element['cart_description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Note on the form of adding to cart'),
      '#default_value' => isset($items[$delta]->cart_description) ? $items[$delta]->cart_description : '',
    ];

    $element['#theme'] = 'site_commerce_product_settings_default';

    return $element;
  }
}

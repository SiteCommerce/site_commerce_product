<?php

namespace Drupal\site_commerce_product\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the Product entities attribute field type.
 *
 * @FieldType(
 *   id = "site_commerce_product_attribute",
 *   label = @Translation("Attribute of product"),
 *   module = "site_commerce_product",
 *   category = @Translation("SiteCommerce"),
 *   translatable = TRUE,
 *   description = @Translation("Display attribute of product."),
 *   default_widget = "site_commerce_product_attribute_default",
 *   cardinality = -1
 * )
 */
class ProductAttribute extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

    $properties['gid'] = DataDefinition::create('string')
      ->setLabel(t('Group ID'))
      ->setRequired(TRUE);

    $properties['aid'] = DataDefinition::create('integer')
      ->setLabel(t('Attribute ID'))
      ->setRequired(TRUE);

    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Value of attribute'))
      ->setRequired(FALSE);

    $properties['quantity_unit'] = DataDefinition::create('string')
      ->setLabel(t('Unit of quantity measurement'))
      ->setRequired(FALSE);

    $properties['image_target_id'] = DataDefinition::create('entity_reference')
      ->setLabel(t('Image'))
      ->setRequired(FALSE);

    $properties['group'] = DataDefinition::create('string')
      ->setLabel(t('Price group'))
      ->setRequired(FALSE);

    $properties['number'] = DataDefinition::create('float')
      ->setLabel(t('Price'))
      ->setRequired(FALSE);

    $properties['currency_code'] = DataDefinition::create('string')
      ->setLabel(t('Letter currency code'))
      ->setRequired(FALSE);

    return $properties;

  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'gid' => [
          'description' => t('Group ID'),
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'default' => '',
        ],
        'aid' => [
          'description' => t('Attribute ID'),
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 0,
        ],
        'value' => [
          'description' => t('Value of attribute'),
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
          'default' => '',
        ],
        'image_target_id' => [
          'description' => t('Image'),
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => FALSE,
          'default' => 0,
        ],
        'quantity_unit' => [
          'description' => t('Unit of quantity measurement'),
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
          'default' => '',
        ],
        'group' => [
          'description' => t('Price group'),
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
          'default' => '',
        ],
        'number' => [
          'description' => t('Price'),
          'type' => 'numeric',
          'precision' => 65,
          'scale' => 30,
          'unsigned' => TRUE,
          'not null' => FALSE,
          'default' => 0,
        ],
        'currency_code' => [
          'description' => t('Letter currency code'),
          'type' => 'varchar',
          'length' => 3,
          'not null' => FALSE,
          'default' => '',
        ],
      ],
      'indexes' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $isEmpty = FALSE;
    if (!$this->get('aid')->getValue()) {
      $isEmpty = TRUE;
    }
    return $isEmpty;
  }

}

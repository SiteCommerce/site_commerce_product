<?php

namespace Drupal\site_commerce_product\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\file\Entity\File;
use Drupal\media\MediaInterface;

/**
 * Plugin implementation of the media image formatter.
 *
 * @FieldFormatter(
 *   id = "site_commerce_product_flickity_media_image",
 *   label = @Translation("Flickity product slider"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class ProductFlickityMediaImage extends EntityReferenceFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    $media_items = $this->getEntitiesToView($items, $langcode);

    // Early opt-out if the field is empty.
    if (empty($media_items)) {
      return $elements;
    }

    $style = NULL;

    $data = [];

    /** @var \Drupal\media\MediaInterface[] $media_items */
    foreach ($media_items as $delta => $media) {
      // Only handle media objects.
      if ($media instanceof MediaInterface) {

        // Загружаем стиль изображения.
        if (!$style) {
          $display_options = EntityViewDisplay::collectRenderDisplay($media, $media->getEntityTypeId())->getComponent('field_media_image');
          if ($display_options['type'] == 'image') {
            $image_style_id = $display_options['settings']['image_style'];
            $style = \Drupal::entityTypeManager()->getStorage('image_style')->load($image_style_id);
          }
        }

        // Получаем идентификатор файла.
        $value = $media->getSource()->getSourceFieldValue($media);
        if (is_numeric($value) && $style) {
          $file = File::load($value);
          if (!empty($file)) {
            $uri = $file->getFileUri();
            if (!empty($uri)) {
              $data[$delta] = [
                'url' => file_create_url($uri),
                'style_url' => $style->buildUrl($uri),
              ];
            }
          }
        }
      }
    }

    $elements = [
      '#theme' => 'site_commerce_product_flickity_media_image',
      '#data' => $data,
    ];

    return $elements;
  }
}

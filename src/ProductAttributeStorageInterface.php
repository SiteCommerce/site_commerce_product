<?php

namespace Drupal\site_commerce_product;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\site_commerce_product\Entity\ProductAttributeInterface;
use Drupal\site_commerce_product\Entity\ProductInterface;

/**
 * Defines an interface for attribute entity storage classes.
 */
interface ProductAttributeStorageInterface extends ContentEntityStorageInterface {

  /**
   * Finds all parents of a given attribute ID.
   *
   * @param int $aid
   *   Attribute ID to retrieve parents for.
   *
   * @return \Drupal\site_commerce_product\Entity\ProductAttributeInterface[]
   *   An array of attribute objects which are the parents of the attribute $aid.
   */
  public function loadParents($aid);

  /**
   * Finds all children of a attribute ID.
   *
   * @param int $aid
   *   Attribute ID to retrieve children for.
   *
   * @return \Drupal\site_commerce_product\Entity\ProductAttributeInterface[]
   *   An array of attribute objects that are the children of the attribute $aid.
   */
  public function loadChildren($aid);

  /**
   * Finds all attributes in a given group ID.
   *
   * @param string $gid
   *   Group ID to retrieve attributes for.
   * @param int $parent
   *   The attribute ID under which to generate the tree. If 0, generate the tree
   *   for the entire group.
   * @param int $max_depth
   *   The number of levels of the tree to return. Leave NULL to return all
   *   levels.
   * @param bool $load_entities
   *   If TRUE, a full entity load will occur on the attribute objects. Defaults to FALSE.
   *
   * @return object[]|\Drupal\site_commerce_product\Entity\ProductAttributeInterface[]
   *   An array of attribute objects that are the children of the group $gid.
   */
  public function loadTree($gid, $parent = 0, $max_depth = NULL, $load_entities = FALSE);

  /**
   * Finds links with attribute ID and product ID.
   *
   * @param \Drupal\site_commerce_product\Entity\ProductAttributeInterface $attribute
   *   Attribute entity.
   *
   * @param \Drupal\site_commerce_product\Entity\ProductInterface $entity
   *   Product entity.
   *
   * @return bool
   *   TRUE if link exist
   */
  public function existLinkWithProduct(ProductAttributeInterface $attribute, ProductInterface $entity);

}

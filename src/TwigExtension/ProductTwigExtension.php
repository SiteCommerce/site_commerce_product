<?php

namespace Drupal\site_commerce_product\TwigExtension;

use Drupal\site_commerce_product\Form\ProductLoadByCategoriesForm;
use Drupal\site_commerce_product\Controller\ProductController;
use Drupal\site_commerce_product\Controller\ProductCatalogController;

/**
 * Twig extension that adds a custom function and a custom filter.
 */
class ProductTwigExtension extends \Twig_Extension {

  /**
   * In this function we can declare the extension function
   */
  public function getFunctions() {
    return array(
      new \Twig_SimpleFunction('site_commerce_product_catalog', array($this, 'catalog')),
      new \Twig_SimpleFunction('site_commerce_product_catalog_list', array($this, 'catalogList')),
      new \Twig_SimpleFunction('site_commerce_product_catalog_children_list', array($this, 'catalogChildrenList')),
      new \Twig_SimpleFunction('site_commerce_product_load_by_categories_form', array($this, 'loadByCategoriesForm')),
      new \Twig_SimpleFunction('site_commerce_product_price_editor', array($this, 'priceEditor')),
      new \Twig_SimpleFunction('site_commerce_product_status_stock', array($this, 'statusStock')),
      new \Twig_SimpleFunction('site_commerce_product_export_price', array($this, 'exportPrice')),
    );
  }

  /**
   * Gets a unique identifier for this Twig extension.
   *
   * @return string
   *   A unique identifier for this Twig extension.
   */
  public function getName() {
    return 'site_commerce_product.twig_extension';
  }

  /**
   * Корневые категории товаров.
   */
  public static function catalog(string $vid = "site_commerce_catalog") {
    return ProductCatalogController::catalog($vid);
  }

  /**
   * Корневые категории товаров в виде списка.
   */
  public static function catalogList(string $vid = "site_commerce_catalog") {
    return ProductCatalogController::catalogList($vid);
  }

  /**
   * Дочерние категории товаров в виде списка.
   */
  public static function catalogChildrenList(int $tid) {
    return ProductCatalogController::catalogChildrenList($tid);
  }

  /**
   * Формирует форму для загрузки товаров по выбранным категориям.
   */
  public static function loadByCategoriesForm(int $parent = 0) {
    $form = new ProductLoadByCategoriesForm($parent);
    $form = \Drupal::formBuilder()->getForm($form);
    return $form;
  }

  /**
   * Редактор цен по выбранной категории товаров.
   */
  public static function priceEditor(int $tid) {
    return ProductController::priceEditor($tid);
  }

  /**
   * Возвращает строковое значение статуса товара на складе.
   */
  public static function statusStock($site_commerce_product) {
    return ProductController::productStatusStock($site_commerce_product);
  }

  /**
   * Возвращает кнопку на скачивание прайс листа по выбранной категории.
   */
  public static function exportPrice(int $tid) {
    return ProductController::exportPrice($tid);
  }
}

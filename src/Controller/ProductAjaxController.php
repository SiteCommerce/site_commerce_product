<?php

/**
 * @file
 * Contains \Drupal\site_commerce_product\Controller\ProductAjaxController
 */

namespace Drupal\site_commerce_product\Controller;

use Drupal\Component\Utility\Html;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CssCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\site_commerce_product\Controller\ProductDatabaseController;
use Drupal\site_commerce_product\Entity\ProductInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProductAjaxController extends ControllerBase {

  /**
   * The servises classes.
   *
   * @var \Drupal\site_commerce_product\Controller\ProductDatabaseController
   */
  protected $database;

  /**
   * Construct.
   *
   * @param \Drupal\site_commerce_product\Controller\ProductDatabaseController $connection
   *   The database connection.
   */
  public function __construct(ProductDatabaseController $database) {
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('site_commerce_product.database')
    );
  }

  /**
   * Регистрирует изменение стоимости позиции.
   * @param Request $request
   * @param mixed $method
   * @return AjaxResponse
   */
  public function updatePrice(Request $request, $method) {
    // Инициализация AjaxResponse.
    $response = new AjaxResponse();

    // Проверка входных данных.
    $data = $request->request->get('data');
    if ($method != 'ajax') {
      throw new NotFoundHttpException();
    }
    $data = json_decode($data);

    $entity = \Drupal::entityTypeManager()
      ->getStorage('site_commerce_product')
      ->load($data->pid);
    if ($entity instanceof ProductInterface) {
      $number_from = trim($data->number_from);
      $number_from = \Drupal::service('kvantstudio.formatter')->decimal($number_from);

      $number = trim($data->number);
      $number = \Drupal::service('kvantstudio.formatter')->decimal($number);

      // Изменяем стоимость.
      $group = 'retail';
      $values = [
        'group' => $group,
        'number_from' => $number_from,
        'number' => $number,
      ];
      $entity->setPrice($values, $group);

      // Изменяем единицу измерения количества.
      $entity->setQuantityUnit($data->quantity_unit);

      // Сохраняем изменения.
      $entity->save();

      // Изменяем цвет строки.
      $selector = '#product-' . $data->pid;
      $css = array(
        'background-color' => '#00db6a',
      );
      $response->addCommand(new CssCommand($selector, $css));

      // Очищает cache.
      \Drupal::service('cache_tags.invalidator')->invalidateTags([
        'taxonomy_term:' . $data->tid,
        'site_commerce_product:' . $entity->pid,
      ]);
    }

    // Return ajax response.
    return $response;

  }

  /**
   * Регистрирует вес позиций в категории.
   *
   * @param  [string]   $method
   * @param  [int]      $cid
   * @param  [string]   $pids
   *
   * @return ajax response
   *
   * @access public
   */
  public function positionsSetWeight($method, $tid, $pids) {
    if ($method == 'ajax') {
      // Create AJAX Response object.
      $response = new AjaxResponse();

      $pids = explode("&pid=", $pids);
      unset($pids[0]);

      $weight = 1;
      foreach ($pids as $pid) {
        $entity = \Drupal::entityTypeManager()
          ->getStorage('site_commerce_product')
          ->load($pid);
        if ($entity instanceof ProductInterface) {
          $entity->setWeight($weight);

          // Сохраняем изменения.
          $entity->save();

          $weight++;
        }
      }

      // Очищает cache.
      \Drupal::service('cache_tags.invalidator')->invalidateTags([
        'taxonomy_term:' . $tid,
      ]);

      // Return ajax response.
      return $response;
    }
  }

  /**
   * Ajax обработчик загрузки товаров по выбранным категориям.
   * @param Request $request
   * @param mixed $method
   * @return AjaxResponse|void
   */
  public function productsLoadByCategories(Request $request, $method) {
    // Инициализация AjaxResponse.
    $response = new AjaxResponse();

    // Проверка входных данных.
    $data = $request->request->get('data');
    if ($method != 'ajax') {
      throw new NotFoundHttpException();
    }
    $data = json_decode($data);

    if (empty($data)) {
      $session = \Drupal::service('session');
      $data = $session->get('selected_tids');
    }

    $build = [];
    foreach ($data->tids as $tid) {
      $entity = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($tid);
      $build[] = \Drupal::service('site_commerce_product.catalog')->getCatalogByTerm($entity, 0);
    }

    if (count($build)) {
      $id = Html::getId($data->id);
      $response->addCommand(new HtmlCommand('#products-load', $build));
    }

    // Return ajax response.
    return $response;
  }

}

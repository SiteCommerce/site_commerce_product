<?php

namespace Drupal\site_commerce_product\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\site_commerce_product\Entity\ProductAttributeGroupInterface;
use Drupal\site_commerce_product\Entity\ProductAttributeInterface;

/**
 * Provides route responses for taxonomy.module.
 */
class ProductAttributeController extends ControllerBase {

  /**
   * Returns a form to add a new attribute to a group.
   *
   * @param \Drupal\site_commerce_product\Entity\ProductAttributeGroupInterface $site_commerce_attribute_group
   *   The group this attribute will be added to.
   *
   * @return array
   *   The attribute add form.
   */
  public function addForm(ProductAttributeGroupInterface $site_commerce_attribute_group) {
    $attribute = $this->entityTypeManager()->getStorage('site_commerce_attribute')->create(['gid' => $site_commerce_attribute_group->id()]);
    return $this->entityFormBuilder()->getForm($attribute);
  }

  /**
   * Route title callback.
   *
   * @param \Drupal\site_commerce_product\Entity\ProductAttributeGroupInterface $site_commerce_attribute
   *   The attribute type.
   *
   * @return string
   *   The attribute type label as a render array.
   */
  public function groupTitle(ProductAttributeGroupInterface $site_commerce_attribute_group) {
    return ['#markup' => $site_commerce_attribute_group->label(), '#allowed_tags' => Xss::getHtmlTagList()];
  }

  /**
   * Route title callback.
   *
   * @param \Drupal\site_commerce_product\Entity\ProductAttributeInterface $site_commerce_attribute
   *   The attribute.
   *
   * @return array
   *   The attribute label as a render array.
   */
  public function attributeTitle(ProductAttributeInterface $site_commerce_attribute) {
    return ['#markup' => $site_commerce_attribute->getName(), '#allowed_tags' => Xss::getHtmlTagList()];
  }

}

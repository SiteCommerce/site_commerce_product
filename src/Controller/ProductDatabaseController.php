<?php

namespace Drupal\site_commerce_product\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\file\Entity\File;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Database class.
 */
class ProductDatabaseController extends ControllerBase {

  /**
   * The database connection object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs a new ProductDatabaseController.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('database'));
  }

  /**
   * Возвращает количество товаров в зависимости от статуса и категории.
   *
   * @param int $status - статус публикации товара.
   * @param int $tid - категория товара для которой проверяется наличие товаров.
   * @param bool $child - выполнять проверку с учетом подкатегорий.
   * @return int
   */
  public function countProducts(int $status = 1, int $tid = 0, bool $child = TRUE) {
    $query = $this->connection->select('site_commerce_product_field_data', 'n');
    if ($status < 2) {
      $query->condition('n.status', $status);
    }
    $query->addExpression('COUNT(product_id)', 'product_id_count');

    $tids = [];
    if ($term = \Drupal\taxonomy\Entity\Term::load($tid)) {
      if ($term->field_view->value) {
        $tids[] = $term->id();
      }

      if ($child) {
        $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($term->getVocabularyId(), $term->id(), NULL, TRUE);
        foreach ($terms as $term) {
          // Проверяем разрешено или нет отображать текущую категорию в каталоге.
          // Как правило запрещено отображать страницы фильтры по каталогу.
          if ($term->field_view->value) {
            $tids[] = $term->id();
          }
        }
      }

      if (count($tids)) {
        $query->innerJoin('site_commerce_product__field_category', 't', 't.entity_id=n.product_id');
        $query->condition('t.field_category_target_id', $tids, 'IN');
        return $query->execute()->fetchField();
      }
    }

    return 0;
  }

  /**
   * Возвращает объекты товаров в зависимости от статуса и категории.
   *
   * @param int $status
   * @param int $tid
   * @return void
   */
  public function loadProducts($tid, $status = 1) {
    $loadProducts = &drupal_static(__FUNCTION__);
    if (!isset($loadProducts)) {
      $query = $this->connection->select('site_commerce_product_field_data', 'n');
      $query->fields('n', array('product_id', 'title'));
      if ($status < 2) {
        $query->condition('n.status', $status);
      }
      $query->innerJoin('site_commerce_product__field_category', 't', 't.entity_id=n.product_id');
      $query->condition('t.field_category_target_id', $tid);

      $loadProducts = $query->execute()->fetchAllKeyed();
    }
    return $loadProducts;
  }

  /**
   * Обновляет значение параметров в редакторе цен.
   */
  public function priceEditorUpdate($data) {
    $site_commerce_product = \Drupal::entityTypeManager()->getStorage('site_commerce_product')->load($data['product_id']);
    $site_commerce_product->field_price_group->from = $data['from'];
    $site_commerce_product->field_price_group->value = $data['value'];
    $site_commerce_product->field_settings->quantity_unit = $data['unit'];
    $site_commerce_product->save();
  }

  /**
   * Загружает информацию о файле.
   */
  public function loadFile($type, $id) {
    $query = $this->connection->select('file_usage', 'n');
    $query->fields('n', array('fid'));
    $query->condition('n.module', 'site_commerce_product');
    $query->condition('n.type', $type);
    $query->condition('n.id', $id);
    $fid = $query->execute()->fetchField();
    $file = null;
    if ($fid) {
      $file = File::load($fid);
    }
    return $file;
  }
}

<?php

namespace Drupal\site_commerce_product\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\site_commerce_product\Entity\ProductInterface;
use Drupal\site_commerce_product\Entity\ProductTypeInterface;

/** @package Drupal\site_commerce_product\Controller */
class ProductController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays add content links for available content types.
   *
   * Redirects to site_commerce_product/add/[type] if only one content type is available.
   *
   * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
   *   A render array for a list of the site_commerce_product types that can be added; however,
   *   if there is only one site_commerce_product type defined for the site, the function
   *   will return a RedirectResponse to the site_commerce_product add page for that one site_commerce_product
   *   type.
   */
  public function addPage() {
    $content = array();

    foreach (\Drupal::entityTypeManager()->getStorage('site_commerce_product_type')->loadMultiple() as $type) {
      $content[$type->id()] = $type;
    }

    if (!count($content)) {
      return $this->redirect('entity.site_commerce_product_type.add_form');
    }

    if (count($content) == 1) {
      $type = array_shift($content);
      return $this->redirect('entity.site_commerce_product.add_form', ['site_commerce_product_type' => $type->id()]);
    }

    return [
      '#theme' => 'site_commerce_product_add_list',
      '#content' => $content,
    ];
  }

  /**
   * Provides the site_commerce_product submission form.
   *
   * @param \Drupal\site_commerce_product\Entity\ProductTypeInterface $site_commerce_product_type
   *   The site_commerce_product type entity for the site_commerce_product.
   *
   * @return array
   *   A site_commerce_product submission form.
   */
  public function add(ProductTypeInterface $site_commerce_product_type) {
    $site_commerce_product = \Drupal::entityTypeManager()->getStorage('site_commerce_product')->create([
      'type' => $site_commerce_product_type->id(),
    ]);

    $form = $this->entityFormBuilder()->getForm($site_commerce_product);

    return $form;
  }

  /**
   * The _title_callback for the site_commerce_product.add_page route.
   *
   * @param \Drupal\site_commerce_product\Entity\ProductTypeInterface $site_commerce_product_type
   *   The current site_commerce_product.
   *
   * @return string
   *   The page title.
   */
  public function addPageTitle(ProductTypeInterface $site_commerce_product_type) {
    return $this->t('Create @name', array('@name' => $site_commerce_product_type->label()));
  }

  /**
   * The _title_callback for the site_commerce_product.add route.
   *
   * @param \Drupal\site_commerce_product\Product $site_commerce_product
   *   The current site_commerce_product.
   *
   * @return string
   *   The page title.
   */
  public function getProductTitle(ProductInterface $site_commerce_product) {
    return $this->t('@name', array('@name' => $site_commerce_product->label()));
  }

  /**
   * Страница редактора цен.
   */
  public static function priceEditor($tid) {
    $build = [];
    $query = \Drupal::database()->select('site_commerce_product', 'n');
    $query->fields('n', array('product_id'));
    $query->condition('status', 1);
    $query->innerJoin('site_commerce_product_field_data', 'f', 'f.product_id=n.product_id');
    $query->innerJoin('site_commerce_product__field_category', 'c', 'c.entity_id=n.product_id');
    $query->condition('field_category_target_id', $tid);
    $query->orderBy('f.weight');
    $product_ids = $query->execute()->fetchCol();

    $site_commerce_products = \Drupal::entityTypeManager()->getStorage('site_commerce_product')->loadMultiple($product_ids);

    $build['products'] = [
      '#theme' => 'site_commerce_product_price_editor',
      '#site_commerce_products' => $site_commerce_products,
      '#tid' => $tid,
      '#attached' => [
        'library' => [
          'site_commerce_product/administer',
        ],
      ],
    ];

    return $build;
  }

  /**
   * Возвращает темизированный статус товара на складе.
   */
  public static function productStatusStock($entity) {
    $build = [];

    $status = (int) $entity->get('field_settings')->status_stock;
    if ($status) {
      $build['products'] = [
        '#theme' => 'site_commerce_product_status_stock',
        '#entity' => $entity,
        '#status' => $status,
        '#name' => getStockAvailability($status),
      ];
    }

    return $build;
  }

  /**
   * Возвращает форму на скачивание прайс-листа по выбранной категории товаров.
   */
  public static function exportPrice(int $tid) {
    $build = [];

    $form = new \Drupal\site_commerce_product\Form\ProductExportPriceForm($tid);
    $form = \Drupal::formBuilder()->getForm($form, $tid);

    $build = [
      '#theme' => 'site_commerce_product_export_price',
      '#form' => $form,
    ];

    return $build;
  }

  /**
   * Возвращает страницу с характеристиками товаров.
   */
  public static function getCharacteristicsEditor() {
    $build = [];

    $build['characteristics_editor'] = array(
      '#theme' => 'site_commerce_product_characteristics_editor',
      '#attached' => array(
        'library' => array(
          'site_commerce_product/characteristics',
          'site_commerce_product/tooltipster',
        ),
      ),
      '#cache' => [
        'max-age' => 0,
      ],
    );

    return $build;
  }
}

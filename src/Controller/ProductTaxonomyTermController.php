<?php

namespace Drupal\site_commerce_product\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\taxonomy\TermInterface;

/**
 * Class ProductTaxonomyTermController
 */
class ProductTaxonomyTermController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function render(TermInterface $taxonomy_term) {
    if ($taxonomy_term->bundle() == 'site_commerce_catalog') {
      return \Drupal::entityTypeManager()->getViewBuilder('taxonomy_term')->view($taxonomy_term, 'full', NULL);
    } else {
      return views_embed_view('taxonomy_term', 'page_1', $taxonomy_term->id());
    }
  }

}


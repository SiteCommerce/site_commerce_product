<?php

/**
 * @file
 * Contains \Drupal\site_commerce_product\Controller\ProductCatalogController
 */

namespace Drupal\site_commerce_product\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\taxonomy\TermInterface;

/**
 * Класс реализующий функции для работы с каталогом товаров.
 */
class ProductCatalogController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Страница каталога с товарами по категории.
   */
  public static function getCatalogByTerm(TermInterface $term_current, int $limit = 4) {
    $build = [];
    $product_ids = [];
    $products = [];

    // Текущий пользователь.
    $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());

    $database = \Drupal::database();

    // Получаем товары подкатегории.
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree(
      $term_current->bundle(),
      $term_current->id(), NULL, TRUE
    );
    foreach ($terms as $term) {
      $product_ids = [];
      if ($term->field_view->value) {
        $query = $database->select('site_commerce_product_field_data', 'n');

        if (!$user->hasPermission('administer site_commerce_product')) {
          $query->condition('n.status', 1);
          $query->condition('n.status_catalog', 1);
        }

        $query->fields('n', ['product_id']);
        $query->innerJoin('site_commerce_product__field_category', 't', 't.entity_id=n.product_id');
        $query->condition('t.field_category_target_id', $term->id());

        // Если установлено ограничение на загрузку.
        if ($limit) {
          $query->range(0, $limit);
        }

        $query->orderBy('weight');
        $product_ids = $query->execute()->fetchCol();

        if (count($product_ids)) {
          $count_term_products = \Drupal::service('site_commerce_product.database')->countProducts(1, $term->id());
          $products[$term->id()]['count_term_products'] = $count_term_products;

          $products[$term->id()]['view_all'] = FALSE;
          $products[$term->id()]['tid'] = 0;
          if ($limit && $count_term_products > $limit) {
            $products[$term->id()]['view_all'] = TRUE;
            $products[$term->id()]['tid'] = $term->id();
          }

          $products[$term->id()]['name'] = $term->getName();
          $products[$term->id()]['summary'] = $term->get('field_summary_catalog')->value;
          $products[$term->id()]['site_commerce_product'] = \Drupal::entityTypeManager()->getStorage('site_commerce_product')->loadMultiple($product_ids);
        }
      }
    }

    // Товары в текущей категории.
    $product_ids = [];
    $query = $database->select('site_commerce_product_field_data', 'n');

    if (!$user->hasPermission('administer site_commerce_product')) {
      $query->condition('n.status', 1);
      $query->condition('n.status_catalog', 1);
    }

    $query->fields('n', ['product_id']);
    $query->innerJoin('site_commerce_product__field_category', 't', 't.entity_id=n.product_id');
    $query->condition('t.field_category_target_id', $term_current->id());
    $query->orderBy('weight');

    // if ($limit && !count($terms)) {
    //   $pager = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit(40);
    //   $product_ids = $pager->execute()->fetchCol();
    // } else {
    //   $product_ids = $query->execute()->fetchCol();
    // }

    $product_ids = $query->execute()->fetchCol();

    if (count($product_ids)) {
      $count_term_products = \Drupal::service('site_commerce_product.database')->countProducts(1, $term_current->id());
      $products[$term_current->id()]['count_term_products'] = $count_term_products;

      // Заголовок основной категории.
      $products[$term_current->id()]['name'] = '';
      if (!$limit) {
        $products[$term_current->id()]['name'] = $term_current->getName();
      }

      $products[$term_current->id()]['summary'] = $term_current->get('field_summary_catalog')->value;
      $products[$term_current->id()]['view_all'] = FALSE;
      $products[$term_current->id()]['tid'] = 0;
      $products[$term_current->id()]['site_commerce_product'] = \Drupal::entityTypeManager()->getStorage('site_commerce_product')->loadMultiple($product_ids);
    }

    if (count($products)) {
      $build['products'] = [
        '#theme' => 'site_commerce_product_catalog_products',
        '#site_commerce_products' => $products,
        '#attached' => [
          'library' => [
            'site_commerce_product/card',
            'site_commerce_product/catalog_products',
          ],
        ],
        // '#cache' => [
        //   'tags' => ['taxonomy_term:' . $term_current->id(), 'limit_' . $limit],
        //   'contexts' => ['languages'],
        // ],
      ];

      // TODO: удалить если представление будет работать корректно с пейджером.
      // // Добавляет пейджер на страницу.
      // if ($limit && !count($terms)) {
      //   $build['pager'] = [
      //     '#type' => 'pager',
      //     '#quantity' => 2,
      //   ];
      // }
    }

    return $build;
  }

  /**
   * Страница отображения категорий каталога по названию словаря.
   */
  public static function catalog($vid = "site_commerce_catalog") {
    $query = \Drupal::database()->select('taxonomy_term_field_data', 'n');
    $query->fields('n', array('tid'));
    $query->condition('n.vid', $vid);
    $query->orderBy('n.weight', 'ASC');

    $query->innerJoin('taxonomy_term__parent', 'h', 'h.entity_id=n.tid');
    $query->condition('h.parent_target_id', 0);
    $query->condition('h.bundle', 'site_commerce_catalog');

    $query->innerJoin('taxonomy_term__field_view', 'v', 'v.entity_id=n.tid');
    $query->condition('v.bundle', 'site_commerce_catalog');
    $query->condition('v.field_view_value', 1);

    $tids = $query->execute()->fetchCol();

    $terms = \Drupal\taxonomy\Entity\Term::loadMultiple($tids);

    return [
      '#theme' => 'site_commerce_product_catalog',
      '#terms' => $terms,
      '#vid' => $vid,
      '#attached' => [
        'library' => [
          'site_commerce_product/card',
        ],
      ],
    ];
  }

  /**
   * Страница отображения категорий каталога по названию словаря в виде списка.
   */
  public static function catalogList($vid = "site_commerce_catalog") {
    $query = \Drupal::database()->select('taxonomy_term_field_data', 'n');
    $query->fields('n', array('tid'));
    $query->condition('n.vid', $vid);
    $query->orderBy('n.weight', 'ASC');
    $query->innerJoin('taxonomy_term__parent', 'h', 'h.entity_id=n.tid');
    $query->condition('h.parent_target_id', 0);
    $query->condition('h.bundle', 'site_commerce_catalog');
    $tids = $query->execute()->fetchCol();
    $terms = \Drupal\taxonomy\Entity\Term::loadMultiple($tids);

    return [
      '#theme' => 'site_commerce_product_catalog_list',
      '#terms' => $terms,
      '#vid' => $vid,
      '#attached' => [
        'library' => [
          'site_commerce_product/card',
        ],
      ],
    ];
  }

  /**
   * Список дочерних категорий по номеру категории-родителя.
   */
  public static function catalogChildrenList($tid) {
    $db = \Drupal::database();

    $term = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($tid);

    $terms_result = [];

    // Дочерние термины для которого получаем перечень товаров.
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($term->getVocabularyId(), $term->id(), NULL, TRUE);
    if (count($terms)) {
      foreach ($terms as $term) {
        $product_ids = [];
        if ($term->field_view->value) {
          $query = $db->select('site_commerce_product_field_data', 'n');
          $query->condition('n.status', 1);
          $query->condition('n.status_catalog', 1);
          $query->fields('n', array('product_id'));
          $query->innerJoin('site_commerce_product__field_category', 't', 't.entity_id=n.product_id');
          $query->condition('t.field_category_target_id', $term->id());
          $result = $query->countQuery()->execute()->fetchField();
          if ($result) {
            $terms_result[] = $term;
          }
        }
      }
    }

    return [
      '#theme' => 'site_commerce_product_catalog_children_list',
      '#terms' => $terms_result,
      '#attached' => [
        'library' => [
          'site_commerce_product/card',
        ],
      ],
    ];
  }

}

<?php

namespace Drupal\site_commerce_product\Cache;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CalculatedCacheContextInterface;
use Drupal\Core\Cache\Context\UserCacheContextBase;

class ProductCacheContext extends UserCacheContextBase implements CalculatedCacheContextInterface {

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return $this->t('Product cache context');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext($operation = NULL) {
    return kvantstudio_user_hash();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata($operation = NULL) {
    $cacheable_metadata = new CacheableMetadata();
    return $cacheable_metadata;
  }
}

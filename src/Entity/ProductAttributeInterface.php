<?php

namespace Drupal\site_commerce_product\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface defining a taxonomy term entity.
 */
interface ProductAttributeInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Gets the term description.
   *
   * @return string
   *   The term description.
   */
  public function getDescription();

  /**
   * Sets the term description.
   *
   * @param string $description
   *   The term description.
   *
   * @return $this
   */
  public function setDescription($description);

  /**
   * Gets the term name.
   *
   * @return string
   *   The term name.
   */
  public function getName();

  /**
   * Sets the term name.
   *
   * @param string $name
   *   The term name.
   *
   * @return $this
   */
  public function setName($name);

  /**
   * Gets the term weight.
   *
   * @return int
   *   The term weight.
   */
  public function getWeight();

  /**
   * Sets the term weight.
   *
   * @param int $weight
   *   The term weight.
   *
   * @return $this
   */
  public function setWeight($weight);

  /**
   * Sets the parent attribute.
   *
   * @param \Drupal\site_commerce_product\Entity\ProductAttributeInterface $entity
   *   The attribute entity.
   *
   * @return $this
   */
  public function setParent(ProductAttributeInterface $entity);

  /**
   * Sets the link to product.
   *
   * @param \Drupal\site_commerce_product\Entity\ProductInterface $entity
   *   The product entity.
   *
   * @return $this
   */
  public function setProduct(ProductInterface $entity);

}

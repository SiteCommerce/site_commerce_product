<?php

namespace Drupal\site_commerce_product\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Defines the interface for product attributes group entity.
 */
interface ProductAttributeGroupInterface extends ConfigEntityInterface {

  /**
   * Denotes that no attribute in the group has a parent.
   */
  const HIERARCHY_DISABLED = 0;

  /**
   * Denotes that one or more attributes in the group has a single parent.
   */
  const HIERARCHY_SINGLE = 1;

  /**
   * Denotes that one or more attributes in the group have multiple parents.
   */
  const HIERARCHY_MULTIPLE = 2;

}

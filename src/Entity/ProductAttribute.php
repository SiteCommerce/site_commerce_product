<?php

namespace Drupal\site_commerce_product\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\site_commerce_product\Entity\ProductAttributeInterface;

/**
 * Defines the attribute entity.
 *
 * @ContentEntityType(
 *   id = "site_commerce_attribute",
 *   label = @Translation("Attribute of product"),
 *   label_collection = @Translation("Attributes of product"),
 *   label_singular = @Translation("attribute of product"),
 *   label_plural = @Translation("attributes of product"),
 *   label_count = @PluralTranslation(
 *     singular = "@count attribute of product",
 *     plural = "@count attributes of product",
 *   ),
 *   bundle_label = @Translation("Attributes group"),
 *   handlers = {
 *     "storage" = "Drupal\site_commerce_product\ProductAttributeStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\Core\Entity\EntityListBuilder",
 *     "access" = "Drupal\site_commerce_product\ProductAttributeAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\site_commerce_product\Form\ProductAttributeForm",
 *       "delete" = "Drupal\site_commerce_product\Form\ProductAttributeDeleteForm"
 *     },
 *   },
 *   admin_permission = "administer site_commerce_attribute",
 *   base_table = "site_commerce_attribute",
 *   data_table = "site_commerce_attribute_field_data",
 *   translatable = TRUE,
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id" = "aid",
 *     "bundle" = "gid",
 *     "label" = "name",
 *     "langcode" = "langcode",
 *     "uuid" = "uuid",
 *     "published" = "status",
 *   },
 *   bundle_entity_type = "site_commerce_attribute_group",
 *   field_ui_base_route = "entity.site_commerce_attribute_group.overview_form",
 *   common_reference_target = TRUE,
 *   links = {
 *     "canonical" = "/product-attribute/{site_commerce_attribute}",
 *     "add-form" = "/admin/site-commerce/products/attributes-groups/attribute/{site_commerce_attribute}/add",
 *     "edit-form" = "/admin/site-commerce/products/attributes-groups/attribute/{site_commerce_attribute}/edit",
 *     "delete-form" = "/admin/site-commerce/products/attributes-groups/attribute/{site_commerce_attribute}/delete",
 *   },
 *   permission_granularity = "bundle",
 * )
 */
class ProductAttribute extends ContentEntityBase implements ProductAttributeInterface {

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);

    // See if any of the attribute's children are about to be become orphans.
    $orphans = [];
    /** @var \Drupal\site_commerce_product\Entity\ProductAttributeInterface $attribute */
    foreach ($entities as $aid => $attribute) {
      if ($children = $storage->getChildren($attribute)) {
        /** @var \Drupal\site_commerce_product\Entity\ProductAttributeInterface $child */
        foreach ($children as $child) {
          $parent = $child->get('parent');
          // Update child parents item list.
          $parent->filter(function ($item) use ($aid) {
            return $item->target_id != $aid;
          });

          // If the attribute has multiple parents, we don't delete it.
          if ($parent->count()) {
            $child->save();
          }
          else {
            $orphans[] = $child;
          }
        }
      }
    }

    if (!empty($orphans)) {
      $storage->delete($orphans);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    if (!$this->get('parent')->count()) {
      $this->parent->target_id = 0;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getChangedTime() {
    return $this->get('changed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setChangedTime($timestamp) {
    $this->set('changed', $timestamp);
    return $this;
  }

  /**
   * Returns the timestamp of the last entity change across all translations.
   *
   * @return int
   *   The timestamp of the last entity save operation across all
   *   translations.
   */
  public function getChangedTimeAcrossTranslations() {
    $changed = $this->getUntranslated()->getChangedTime();
    foreach ($this->getTranslationLanguages(FALSE) as $language) {
      $translation_changed = $this->getTranslation($language->getId())->getChangedTime();
      $changed = max($translation_changed, $changed);
    }
    return $changed;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('published');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published = NULL) {
    $this->set('status', 1);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setUnpublished() {
    $this->set('status', 0);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->get('description')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->set('description', $description);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->label();
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return $this->get('weight')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight($weight) {
    $this->set('weight', $weight);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setParent(ProductAttributeInterface $entity) {
    $this->parent[] = $entity->id();
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setProduct(ProductInterface $entity) {
    $storage = \Drupal::entityTypeManager()->getStorage($this->getEntityTypeId());

    $exist = $storage->existLinkWithProduct($this, $entity);
    if (!$exist) {
      $this->product[] = $entity->id();
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    /** @var \Drupal\Core\Field\BaseFieldDefinition[] $fields */
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['gid']->setLabel(t('Group name'))
      ->setDescription(t('The attributes group to which the attribute is assigned.'))
      ->setRequired(TRUE);

    $fields['parent'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Attribute parent'))
      ->setDescription(t('The parent of this attribute.'))
      ->setRequired(FALSE)
      ->setSetting('target_type', 'site_commerce_attribute')
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED);

    $fields['product'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Product'))
      ->setDescription(t('The product ID.'))
      ->setRequired(FALSE)
      ->setSetting('target_type', 'site_commerce_product')
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setTranslatable(TRUE)
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['description'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Description'))
      ->setTranslatable(TRUE)
      ->setRequired(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['image'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Image'))
      ->setDescription(t('Image of the product variant when selecting an attribute.'))
      ->setSetting('target_type', 'media')
      ->setRequired(FALSE)
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('form', [
        'type' => 'media_library_widget',
        'weight' => 2,
        'settings' => [
          'display_label' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['type'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Type of attribute'))
      ->setSetting('allowed_values_function', ['\Drupal\site_commerce_product\Entity\ProductAttribute', 'getTypes'])
      ->setRequired(FALSE)
      ->setDefaultValue('')
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 3,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['value'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Value'))
      ->setTranslatable(TRUE)
      ->setRequired(FALSE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['quantity_unit'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Unit of quantity measurement'))
      ->setSetting('allowed_values_function', ['\Drupal\site_commerce_product\Entity\ProductAttribute', 'getQuantityUnits'])
      ->setRequired(FALSE)
      ->setDefaultValue('')
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['price'] = BaseFieldDefinition::create('site_commerce_price_group')
      ->setLabel(t('Price'))
      ->setDescription(t('Cost per unit.'))
      ->setRequired(FALSE)
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('form', [
        'type' => 'site_commerce_price_group_default',
        'weight' => 6,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Allow displaying in the product card'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 7,
        'settings' => [
          'display_label' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['filter'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Allow displaying as a filter when searching'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 8,
        'settings' => [
          'display_label' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['page'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Allow creating a page with a list of products'))
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 9,
        'settings' => [
          'display_label' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['data'] = BaseFieldDefinition::create('map')
      ->setLabel(t('Data'))
      ->setDescription(t('A serialized array of additional data.'));

    $fields['weight'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Weight'))
      ->setDescription(t('The weight of this attribute in relation to other attributes.'))
      ->setDefaultValue(0);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the attribute was last edited.'))
      ->setTranslatable(TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions) {
    // Only attributes in the same bundle can be a parent.
    $fields['parent'] = clone $base_field_definitions['parent'];
    $fields['parent']->setSetting('handler_settings', ['target_bundles' => [$bundle => $bundle]]);
    return $fields;
  }

  /**
   * Gets the allowed types of attributes.
   * TODO: Support for attribute types will be implemented in future versions.
   *
   * @return array
   *   The allowed values.
   */
  public static function getTypes() {
    return [];
  }

  /**
   * Gets the allowed quantity units.
   *
   * @return array
   *   The allowed values.
   */
  public static function getQuantityUnits() {
    return getUnitMeasurement();
  }

}

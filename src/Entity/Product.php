<?php

namespace Drupal\site_commerce_product\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\user\UserInterface;

/**
 * Defines the Product entity.
 *
 * @ingroup site_commerce_product
 *
 * @ContentEntityType(
 *   id = "site_commerce_product",
 *   label = @Translation("Product"),
 *   label_singular = @Translation("product"),
 *   label_plural = @Translation("products"),
 *   label_count = @PluralTranslation(
 *     singular = "@count product",
 *     plural = "@count products"
 *   ),
 *   bundle_label = @Translation("Product type"),
 *   handlers = {
 *     "view_builder" = "Drupal\site_commerce_product\ProductViewBuilder",
 *     "list_builder" = "Drupal\site_commerce_product\ProductListBuilder",
 *     "views_data" = "Drupal\site_commerce_product\ProductViewsData",
 *     "translation" = "Drupal\site_commerce_product\ProductTranslationHandler",
 *     "form" = {
 *       "default" = "Drupal\site_commerce_product\Form\ProductForm",
 *       "add" = "Drupal\site_commerce_product\Form\ProductForm",
 *       "edit" = "Drupal\site_commerce_product\Form\ProductForm",
 *       "duplicate" = "Drupal\site_commerce_product\Form\ProductDuplicateForm",
 *       "delete" = "Drupal\site_commerce_product\Form\ProductDeleteForm",
 *     },
 *     "access" = "Drupal\site_commerce_product\ProductAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\site_commerce_product\ProductHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "site_commerce_product",
 *   data_table = "site_commerce_product_field_data",
 *   permission_granularity = "bundle",
 *   admin_permission = "administer site_commerce_product",
 *   fieldable = TRUE,
 *   translatable = TRUE,
 *   entity_keys = {
 *     "id" = "product_id",
 *     "bundle" = "type",
 *     "langcode" = "langcode",
 *     "label" = "title",
 *     "uuid" = "uuid",
 *     "uid" = "uid",
 *     "owner" = "uid",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/product/{site_commerce_product}",
 *     "add-page" = "/admin/site-commerce/products/add",
 *     "add-form" = "/admin/site-commerce/products/add/{site_commerce_product_type}",
 *     "edit-form" = "/admin/site-commerce/products/{site_commerce_product}/edit",
 *     "duplicate-form" = "/admin/site-commerce/products/{site_commerce_product}/duplicate",
 *     "delete-form" = "/admin/site-commerce/products/{site_commerce_product}/delete",
 *     "collection" = "/admin/site-commerce/products",
 *   },
 *   bundle_entity_type = "site_commerce_product_type",
 *   field_ui_base_route = "entity.site_commerce_product_type.edit_form",
 * )
 */
class Product extends ContentEntityBase implements ProductInterface {

  use EntityChangedTrait;

  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   *
   * When a new entity instance is added, set the user entity reference to
   * the current user as the creator of the instance.
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'uid' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function preDelete(EntityStorageInterface $storage, array $entities) {
    parent::preDelete($storage, $entities);
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);

    // Удаляем все товары из поискового индекса.
    if (\Drupal::moduleHandler()->moduleExists('search')) {
      foreach ($entities as $entity) {
        \Drupal::service('search.index')->clear('site_commerce_product_search', $entity->id());
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return $this->bundle();
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getChangedTime() {
    return $this->get('changed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setChangedTime($timestamp) {
    $this->set('changed', $timestamp);
    return $this;
  }

  /**
   * Returns the timestamp of the last entity change across all translations.
   *
   * @return int
   *   The timestamp of the last entity save operation across all
   *   translations.
   */
  public function getChangedTimeAcrossTranslations() {
    $changed = $this->getUntranslated()->getChangedTime();
    foreach ($this->getTranslationLanguages(FALSE) as $language) {
      $translation_changed = $this->getTranslation($language->getId())->getChangedTime();
      $changed = max($translation_changed, $changed);
    }
    return $changed;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('published');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published = NULL) {
    $this->set('status', PRODUCT_PUBLISHED);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setUnpublished() {
    $this->set('status', PRODUCT_NOT_PUBLISHED);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPrice($get_raw_value = FALSE, string $group = '') {
    return site_commerce_product_get_cost($this, $get_raw_value, $group);
  }

  /**
   * {@inheritdoc}
   */
  public function getStockAllow() {
    return $this->get('field_settings')->quantity_stock_allow;
  }

  /**
   * {@inheritdoc}
   */
  public function getStockValue() {
    return $this->get('field_settings')->quantity_stock_value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStockValue(int $value) {
    $this->field_settings->quantity_stock_value = $value;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuantityUnit() {
    return $this->get('field_settings')->quantity_unit;
  }

  /**
   * {@inheritdoc}
   */
  public function setQuantityUnit(string $unit) {
    $this->field_settings->quantity_unit = $unit;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuantityOrderMin() {
    return $this->get('field_settings')->quantity_order_min;
  }

  /**
   * {@inheritdoc}
   */
  public function setPrice(array $values, string $group) {
    $groups = $this->field_price_group->getValue();
    if (count($groups)) {
      foreach ($groups as $delta => $item) {
        if ($item['group'] == $group) {
          $data = array_replace($item, $values);
          $this->field_price_group[$delta] = $data;
        }
      }
    } else {
      $this->field_price_group[] = $values;
    }


    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight(int $weight) {
    $this->set('weight', $weight);
    return $this;
  }

  /**
   * {@inheritdoc}
   *
   * Define the field properties here.
   *
   * Field name, type and size determine the table structure.
   *
   * In addition, we can define how the field and its content can be manipulated
   * in the GUI. The behaviour of the widgets used can be determined here.
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['parent'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Base product'))
      ->setDescription(t('A product that includes the current product and is a variant of it.'))
      ->setSetting('target_type', 'site_commerce_product')
      ->setReadOnly(TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Representative of the seller'))
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback('Drupal\site_commerce_product\Entity\Product::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 0,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Allow displaying a product'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 0,
        'settings' => [
          'display_label' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['status_catalog'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Allow displaying a product in the catalog'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 0,
        'settings' => [
          'display_label' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the product was created.'))
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the product was last edited.'))
      ->setTranslatable(TRUE);

    $fields['weight'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Weight'))
      ->setDescription(t('Weight of product in catalog.'))
      ->setDefaultValue(0);

    return $fields;
  }

  /**
   * Default value callback for 'uid' base field definition.
   *
   * @see ::baseFieldDefinitions()
   *
   * @return array
   *   An array of default values.
   */
  public static function getCurrentUserId() {
    return array(\Drupal::currentUser()->id());
  }
}

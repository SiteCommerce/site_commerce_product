<?php

namespace Drupal\site_commerce_product\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Product entities.
 *
 * @ingroup site_commerce_product
 */
interface ProductInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Gets the product name.
   *
   * @return string
   *   Name of the Product entity.
   */
  public function getTitle();

  /**
   * Sets the product name.
   *
   * @param string $name
   *   The product name.
   *
   * @return \Drupal\site_commerce_product\Entity\ProductInterface
   *   The called Product entity.
   */
  public function setTitle($title);

  /**
   * Gets the product creation timestamp.
   *
   * @return int
   *   Creation timestamp of the product.
   */
  public function getCreatedTime();

  /**
   * Sets the product creation timestamp.
   *
   * @param int $timestamp
   *   The product creation timestamp.
   *
   * @return \Drupal\site_commerce_product\Entity\ProductInterface
   *   The called Product entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Get the product price.
   *
   * @return int
   *   Creation timestamp of the product.
   */
  public function getPrice(bool $get_raw_value = FALSE, string $group = '');

  /**
   * Get the stock status view.
   *
   * @return int
   *   Stock status.
   */
  public function getStockAllow();

  /**
   * Get the balance of products in stock.
   *
   * @return int
   *   Count of the product.
   */
  public function getStockValue();

  /**
   * Sets the balance of products in stock.
   *
   * @param int $value
   *   The count of products in stock.
   *
   * @return \Drupal\site_commerce_product\Entity\ProductInterface
   *   The called Product entity.
   */
  public function setStockValue(int $value);

  /**
   * Get the quantity unit.
   *
   * @return string
   *   Quantity unit of the product.
   */
  public function getQuantityUnit();

  /**
   * Sets the unit of quantity.
   *
   * @param string $unit
   *   The unit of quantity.
   *
   * @return \Drupal\site_commerce_product\Entity\ProductInterface
   *   The called Product entity.
   */
  public function setQuantityUnit(string $unit);

  /**
   * Get the quantity order minimum value.
   *
   * @return int
   *   Quantity order minimum value.
   */
  public function getQuantityOrderMin();

  /**
   * Sets the data of price.
   *
   * @param array $values
   *   The data of price.
   *
   * @return \Drupal\site_commerce_product\Entity\ProductInterface
   *   The called Product entity.
   */
  public function setPrice(array $values, string $group);

  /**
   * Sets the weight of products in catalog.
   *
   * @param int $weight
   *   The weight of products.
   *
   * @return \Drupal\site_commerce_product\Entity\ProductInterface
   *   The called Product entity.
   */
  public function setWeight(int $weight);
}

<?php

namespace Drupal\site_commerce_product\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a Product type entities.
 * @ingroup site_commerce_product
 */
interface ProductTypeInterface extends ConfigEntityInterface {

}

<?php

namespace Drupal\site_commerce_product\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\site_commerce_product\Entity\ProductAttributeGroupInterface;

/**
 * Defines the product attribute group entity.
 *
 * @ConfigEntityType(
 *   id = "site_commerce_attribute_group",
 *   label = @Translation("Attributes group"),
 *   label_singular = @Translation("attributes group"),
 *   label_plural = @Translation("attributes groups"),
 *   label_collection = @Translation("Attributes groups"),
 *   label_count = @PluralTranslation(
 *     singular = "@count attributes group",
 *     plural = "@count attributes groups"
 *   ),
 *   handlers = {
 *     "storage" = "Drupal\site_commerce_product\ProductAttributeGroupStorage",
 *     "list_builder" = "Drupal\site_commerce_product\ProductAttributeGroupListBuilder",
 *     "access" = "Drupal\site_commerce_product\ProductAttributeGroupAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\site_commerce_product\Form\ProductAttributeGroupForm",
 *       "edit" = "Drupal\site_commerce_product\Form\ProductAttributeGroupForm",
 *       "delete" = "Drupal\site_commerce_product\Form\ProductAttributeGroupDeleteForm",
 *       "overview" = "Drupal\site_commerce_product\Form\ProductAttributeGroupOverviewForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\site_commerce_product\ProductAttributeGroupRouteProvider",
 *     }
 *   },
 *   admin_permission = "administer site_commerce_attribute",
 *   config_prefix = "site_commerce_attribute_group",
 *   bundle_of = "site_commerce_attribute",
 *   entity_keys = {
 *     "id" = "gid",
 *     "label" = "name",
 *     "weight" = "weight"
 *   },
 *   links = {
 *     "add-form" = "/admin/site-commerce/products/attributes-groups/add",
 *     "delete-form" = "/admin/site-commerce/products/attributes-groups/{site_commerce_attribute_group}/delete",
 *     "overview-form" = "/admin/site-commerce/products/attributes-groups/{site_commerce_attribute_group}/overview",
 *     "edit-form" = "/admin/site-commerce/products/attributes-groups/{site_commerce_attribute_group}/edit",
 *     "collection" = "/admin/site-commerce/products/attributes-groups",
 *   },
 *   config_export = {
 *     "gid",
 *     "parent",
 *     "name",
 *     "description",
 *     "filter",
 *     "expanded",
 *     "weight",
 *   }
 * )
 */
class ProductAttributeGroup extends ConfigEntityBundleBase implements ProductAttributeGroupInterface {

  /**
   * The group ID.
   *
   * @var string
   */
  protected $gid;

  /**
   * The parent group ID.
   *
   * @var string
   */
  protected $parent;

  /**
   * The name of the group.
   *
   * @var string
   */
  protected $name;

  /**
   * The description of the group.
   *
   * @var string
   */
  protected $description;

  /**
   * Allow displaying as a filter when searching.
   *
   * @var bool
   */
  protected $filter = TRUE;

  /**
   * Allow to display the list of attributes in the group expanded.
   *
   * @var bool
   */
  protected $expanded;

  /**
   * The weight of this group in relation to other groups.
   *
   * @var int
   */
  protected $weight = 0;

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->gid;
  }

  /**
   * {@inheritdoc}
   */
  public function getParent() {
    return $this->parent;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return (int) $this->weight;
  }

  /**
   * {@inheritdoc}
   */
  public function getFilter() {
    return (bool) $this->filter;
  }

  /**
   * {@inheritdoc}
   */
  public function getExpanded() {
    return (bool) $this->expanded;
  }

  /**
   * {@inheritdoc}
   */
  public static function preDelete(EntityStorageInterface $storage, array $entities) {
    parent::preDelete($storage, $entities);

    // Only load attributes without a parent, child attributes will get deleted too.
    $attribute_storage = \Drupal::entityTypeManager()->getStorage('site_commerce_attribute');
    $attributes = $attribute_storage->loadMultiple($storage->getToplevelAttributes(array_keys($entities)));
    $attribute_storage->delete($attributes);
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);

    // Reset caches.
    $storage->resetCache(array_keys($entities));

    if (reset($entities)->isSyncing()) {
      return;
    }

    $groups = [];
    foreach ($entities as $group) {
      $groups[$group->id()] = $group->id();
    }

    // Load all attributes fields and delete those which use only this group.
    $field_storages = \Drupal::entityTypeManager()->getStorage('field_storage_config')->loadByProperties(['module' => 'site_commerce_product']);
    foreach ($field_storages as $field_storage) {
      $modified_storage = FALSE;
      // Attribute reference fields may reference attributes from more than one group.
      foreach ($field_storage->getSetting('allowed_values') as $key => $allowed_value) {
        if (isset($groups[$allowed_value['group']])) {
          $allowed_values = $field_storage->getSetting('allowed_values');
          unset($allowed_values[$key]);
          $field_storage->setSetting('allowed_values', $allowed_values);
          $modified_storage = TRUE;
        }
      }
      if ($modified_storage) {
        $allowed_values = $field_storage->getSetting('allowed_values');
        if (empty($allowed_values)) {
          $field_storage->delete();
        }
        else {
          // Update the field definition with the new allowed values.
          $field_storage->save();
        }
      }
    }
  }

}

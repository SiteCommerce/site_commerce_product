<?php

namespace Drupal\site_commerce_product;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the Product entities.
 *
 * @see \Drupal\site_commerce_product\Entity\Product.
 */
class ProductAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   *
   * Link the activities to the permissions. checkAccess is called with the
   * $operation as defined in the routing.yml file.
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    switch ($operation) {
    case 'view':
      return AccessResult::allowedIfHasPermission($account, 'access content');

    case 'edit':
      return AccessResult::allowedIfHasPermission($account, 'administer site_commerce_product');

    case 'update':
      return AccessResult::allowedIfHasPermission($account, 'administer site_commerce_product');

    case 'duplicate':
      return AccessResult::allowedIfHasPermission($account, 'administer site_commerce_product');

    case 'delete':
      return AccessResult::allowedIfHasPermission($account, 'administer site_commerce_product');
    }

    return AccessResult::forbidden();
  }

  /**
   * {@inheritdoc}
   *
   * Separate from the checkAccess because the entity does not yet exist, it
   * will be created during the 'add' process.
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'administer site_commerce_product');
  }

}

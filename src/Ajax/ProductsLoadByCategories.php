<?php

namespace Drupal\site_commerce_product\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * Load products by categories.
 *
 * This command is implemented in Drupal.AjaxCommands.prototype.productsLoadByCategories.
 */
class ProductsLoadByCategories implements CommandInterface {

  /**
   * ID of the HTML element to insert data in.
   *
   * @var string
   */
  protected $id;

  /**
   * Assigned taxonomy terms id.
   *
   * @var array
   */
  protected $tids;

  /**
   * Constructs a \Drupal\site_commerce_product\Ajax\ProductsLoadByCategories object.
   */
  public function __construct(string $id, array $tids) {
    $this->id = $id;
    $this->tids = $tids;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      'command' => 'productsLoadByCategories',
      'id' => $this->id,
      'tids' => $this->tids,
    ];
  }

}

<?php

namespace Drupal\site_commerce_product;

use Drupal\Component\Transliteration\TransliterationInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\file\FileInterface;

class ProductFilenamePostprocessor {

  protected $configFactory;
  protected $transliteration;

  public function __construct(ConfigFactoryInterface $config_factory, TransliterationInterface $transliteration) {
    $this->configFactory = $config_factory;
    $this->transliteration = $transliteration;
  }

  /**
   * Формирует новое уникальное имя файла товара.
   * @param File $entity
   * @return string
   */
  public function process(FileInterface $entity) {
    $filename = $entity->getFilename();
    $extension = $this->get_file_extension($filename);
    $filename = 'товар-' . $entity->uuid() . "." . $extension;
    $filename = $this->transliteration->transliterate($filename);

    return $filename;
  }

  /**
   * Возвращает расширение файла.
   * @param string $filename
   * @return string|false
   */
  public function get_file_extension(string $filename) {
    return substr(strrchr($filename, '.'), 1);
  }
}

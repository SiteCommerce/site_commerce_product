<?php

/**
 * @file
 * Contains \Drupal\site_commerce_product\Breadcrumb\CatalogBreadcrumbBuilder.
 */

namespace Drupal\site_commerce_product\Breadcrumb;

use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;

/**
 * Class to define the Product entities pages breadcrumb builder.
 */
class CatalogBreadcrumbBuilder implements BreadcrumbBuilderInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The taxonomy storage.
   *
   * @var \Drupal\Taxonomy\TermStorageInterface
   */
  protected $termStorage;

  /**
   * Constructs the MediaGalleryCatalogBreadcrumbBuilder.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity manager.
   */
  public function __construct(EntityTypeManager $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->termStorage = $entityTypeManager->getStorage('taxonomy_term');
  }

  /**
   * @inheritdoc
   */
  public function applies(RouteMatchInterface $route_match) {
    if ($route_match->getRouteName() == 'entity.taxonomy_term.canonical') {
      $term = $route_match->getParameter('taxonomy_term');
      if ($term->bundle() == 'site_commerce_catalog') {
        return TRUE;
      }
    }
  }

  /**
   * @inheritdoc
   */
  public function build(RouteMatchInterface $route_match) {
    // Загружаем конфигурацию.
    $config = \Drupal::config('site_commerce_product.settings');

    $breadcrumb = new Breadcrumb();
    $breadcrumb->addLink(Link::createFromRoute($this->t('Home'), '<front>'));

    // Ссылка на каталог продукции.
    if ($config->get('catalog_title') && $config->get('catalog_url')) {
      $link = Link::fromTextAndUrl($this->t('@value', ['@value' => $config->get('catalog_title')]), Url::fromUserInput('/' . $config->get('catalog_url')));
      $breadcrumb->addLink($link);
    }

    $term = $route_match->getParameter('taxonomy_term');
    if ($term instanceof \Drupal\taxonomy\Entity\Term) {
      $term = \Drupal::service('entity.repository')->getTranslationFromContext($term);

      // @todo This overrides any other possible breadcrumb and is a pure
      // hard-coded presumption. Make this behavior configurable per
      // vocabulary or term.
      $parents = $this->termStorage->loadAllParents($term->id());

      // Remove current term being accessed.
      array_shift($parents);
      foreach (array_reverse($parents) as $parents_term) {
        $parents_term = \Drupal::service('entity.repository')->getTranslationFromContext($parents_term);
        $breadcrumb->addCacheableDependency($parents_term);
        $breadcrumb->addLink(Link::createFromRoute($parents_term->getName(), 'entity.taxonomy_term.canonical', array('taxonomy_term' => $parents_term->id())));
      }
    }

    // This breadcrumb builder is based on a route parameter, and hence it
    // depends on the 'route' cache context.
    $breadcrumb->addCacheContexts(['route']);

    return $breadcrumb;
  }
}

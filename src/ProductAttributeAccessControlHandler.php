<?php

namespace Drupal\site_commerce_product;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the attribute entity entity.
 *
 * @see \Drupal\site_commerce_product\Entity\ProductAttribute
 */
class ProductAttributeAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    switch ($operation) {
    case 'view':
      // TODO: запрещаем просмотр страницы с атрибутом, пока не будет создан механизм отображения
      // товаров по атрибуту для SEO продвижения.
      return AccessResult::allowedIfHasPermission($account, 'administer site_commerce_attribute');

    case 'edit':
      return AccessResult::allowedIfHasPermission($account, 'administer site_commerce_attribute');

    case 'update':
      return AccessResult::allowedIfHasPermission($account, 'administer site_commerce_attribute');

    case 'delete':
      return AccessResult::allowedIfHasPermission($account, 'administer site_commerce_attribute');
    }

    return AccessResult::forbidden();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions($account, ['administer site_commerce_attribute']);
  }

}

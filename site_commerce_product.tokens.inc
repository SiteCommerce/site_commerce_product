<?php

/**
 * @file
 * Tokens for the site_commerce_product module.
 */

/**
 * Implements hook_token_info().
 */
function site_commerce_product_token_info() {
  $info = [];

  $info['tokens']['term']['catalog-url'] = [
    'name' => t("The url of the product catalog"),
  ];

  $info['tokens']['site_commerce_product']['product-url'] = [
    'name' => t("The url of the product page"),
  ];

  return $info;
}

/**
 * Implements hook_tokens().
 */
function site_commerce_product_tokens($type, $tokens, array $data, array $options, \Drupal\Core\Render\BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];

  if ($type == 'site_commerce_product') {
    $config = \Drupal::config('site_commerce_product.settings');

    foreach ($tokens as $name => $original) {
      switch ($name) {
      case 'product-url':
        $replacements[$original] = $config->get('product_url');
        break;
      }
    }
  }

  if ($type == 'term') {
    $config = \Drupal::config('site_commerce_product.settings');

    foreach ($tokens as $name => $original) {
      switch ($name) {
      case 'catalog-url':
        $replacements[$original] = $config->get('catalog_url');
        break;
      }
    }
  }

  return $replacements;
}

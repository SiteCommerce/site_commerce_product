/**
 * @file
 * Misc JQuery scripts in this file
 */
(function($, Drupal, drupalSettings) {

    'use strict';

    // Отображение всплывающих подсказок.
    $('.tooltip-click').tooltipster({
        theme: 'tooltipster-borderless',
        delay: 200,
        contentAsHTML: true,
        interactive: true,
        trigger: 'click',
        side: 'bottom',
    });

})(jQuery, Drupal, drupalSettings);
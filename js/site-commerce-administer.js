/**
 * @file
 * Misc JQuery scripts in this file
 */
(function($, window, Drupal, drupalSettings) {

	'use strict';

	// Обновляет параметры позиции в редакторе цен.
	$('.site-commerce-price-editor__btn').click(function(event) {
		event.preventDefault();

    // Идентификатор категории и товара.
    var tid = parseInt($(this).attr('tid'));
		var pid = parseInt($(this).attr('pid'));

		var number_from = Drupal.checkPlain($('#site-commerce-price-editor__number-from-' + pid).val());
		if (!number_from) {
			number_from = 0;
    }

		var number = Drupal.checkPlain($('#site-commerce-price-editor__number-' + pid).val());
		if (!number) {
			number = 0;
    }

		var quantity_unit = Drupal.checkPlain($('#site-commerce-price-editor__quantity-unit-' + pid).val());

		// Данные передаваемые в запросе.
		var data = {};
		data.tid = tid;
		data.pid = pid;
		data.number_from = number_from;
    data.number = number;
    data.quantity_unit = quantity_unit;

		// Формирует ajax запрос.
		var ajaxObject = Drupal.ajax({
			url: '/ajax-update-price/nojs',
			submit: {
				data: JSON.stringify(data)
			}
    });

    ajaxObject
    .execute()
    .done(function(response) {

    })
    .fail(function(response) {
      let result = JSON.parse(response.responseText);
    });
	});

	// Изменяет вес позиций
	Drupal.behaviors.sortablePositionsAjaxFunctions = {
		attach: function(context, settings) {
			$('.site-commerce-price-editor tbody').sortable({
				helper: fixHelper,
				placeholder: 'ui-state-highlight',
				opacity: 0.9,
				update: function(event, ui) {
					var tid = parseInt($(this).attr('tid'));
					var sorted = $(this).sortable('serialize', { key: 'pid' });

					// Выполняет запрос ajax.
					var ajaxObject = Drupal.ajax({
						url: '/ajax-positions-set-weight/' + tid + '/&' + sorted + '/nojs',
						base: false,
						element: false,
						progress: false
					});
					ajaxObject.execute();
				}
			});
			$('.site-commerce-price-editor tbody').disableSelection();
		}
	};

	var fixHelper = function(e, ui) {
		ui.children().each(function() {
			$(this).width($(this).width());
		});
		return ui;
	};
})(jQuery, window, Drupal, drupalSettings);

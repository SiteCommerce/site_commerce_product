/**
* @file
* JavaScript behaviors for Ajax.
*/
(function($, Drupal) {
	'use strict';

	/**
   * Load products by categories.
   *
   * @see Drupal.AjaxCommands
   */
	if (Drupal.AjaxCommands) {
		Drupal.AjaxCommands.prototype.productsLoadByCategories = function(ajax, response) {
			// Данные передаваемые в запросе.
      var data = {};
      data.id = response.id;
      data.tids = response.tids;

			// Формирует ajax запрос.
			var ajaxObject = Drupal.ajax({
				url: '/ajax-products-load-by-categories/nojs',
				submit: {
					data: JSON.stringify(data)
				}
			});

      // Отправляем запрос на сервер.
			ajaxObject.execute().done(function(receivedHtml) {
				Drupal.attachBehaviors();
			});
		};
	}
})(jQuery, Drupal);
